<!DOCTYPE html>
<html>

<head>
    <title>Globaltestpoint</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <style>
    html,
    body {
        height: 100%;
    }
    
    body {
        margin: 0;
        padding: 0;
        width: 100%;
        color: #B0BEC5;
        display: table;
        font-weight: 100;
        font-family: 'Lato', sans-serif;
    }
    
    .container {
        text-align: center;
        display: table-cell;
        vertical-align: middle;
    }
    
    .content {
        text-align: center;
        display: inline-block;
    }
    
    .title {
        font-size: 72px;
        margin-bottom: 40px;
    }
    
    @media only screen and (max-width: 992px) {
        .title {
            font-size: 50px;
            line-height: 80px;
        }
    }
    
    @media only screen and (max-width: 768px) {
        .title {
            font-size: 40px;
            line-height: 60px;
        }
    }
    
    @media only screen and (max-width: 480px) {
        .title {
            font-size: 30px;
            line-height: 50px;
        }
    }
    </style>
</head>

<body>
    <div class="container">
        <div class="content">
            <div class="title">This website www.globaltestpoint.com is undergoing planned maintenance, please check back shortly" You can contact us on info@globaltestpoint.com</div>
        </div>
    </div>
</body>

</html>
