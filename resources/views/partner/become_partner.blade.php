@extends('master')
@include('_include_angular')
@section('title')
    Apply to Become a Partner
@stop
@section('content')
    @include('testyourself.partials.header')

    <div id="become-partner">
        <div class="container">
            {{-- <div class="heading text-center">
                <h1 class="text-center title">Apply to Become a Partner</h1>
                <p>Tableau views our partners as an extension of our team, playing an integral role in our development and growth. As a member of the Tableau Partner Program your company could benefit from market leading business intelligence solutions.</p>
                <p>Please review below to find the partner type most applicable to you.</p>
            </div>
            <div class="partner-benefit">
                <div class="partner-title">Alliance</div>
                <div class="partner-description">
                    <p>Tableau Alliance Partners help customers determine the right solution to fit their business needs. Tableau’s Alliance partners not only have experience implementing Tableau but often have deep industry expertise and/or broader data integration, governance, or change management backgrounds to help you put in place all of the pieces to support a robust agile BI capability. If you are interested in building a services practice around Tableau, but you are not interested in reselling Tableau licenses, then the Alliance program is for you.</p>
                </div>
            </div>
            <div class="partner-benefit">
                <div class="partner-title">Apply to Alliance Program Download Details</div>
                <div class="partner-description">
                    <p><strong>Reseller</strong></p>
                    <p>Tableau Resellers are technology service and product providers that sell Tableau licenses in addition to their own deployment, implementation and other business intelligence services. Our hundreds of Resellers around the world are well trained and equipped to drive new leads and close new business. As a Reseller, your company purchases Tableau licenses or services at favorable margins and then resells those to customers.</p>
                </div>
            </div>
            <div class="partner-benefit">
                <div class="partner-title">Apply to Reseller Program Download Details</div>
                <div class="partner-description">
                    <p><strong>OEM</strong></p>
                    <p>Tableau’s OEM Partner Program solves a common problem faced by software product vendors. You want to embed analytics and reporting into your application, but you find conventional visualization tools too complex, charting packages too limited and business intelligence applications too expensive and hard to use. By becoming a Tableau OEM Partner, you can quickly and easily embed Tableau’s powerful visual data analysis tools into your application.</p>
                </div>
            </div>
            <div class="partner-benefit">
                <div class="partner-title">Apply to OEM Program Download Details</div>
                <div class="partner-description">
                    <p><strong>Technology</strong></p>
                    <p>Tableau’s Technology Partner program is focused on creating an ecosystem of best-of-breed organizations with the ultimate goal of making it simple for the everyday business user to access, integrate, analyze, and share interactive visual analytics. If you build technology or host services designed to help people and organizations with data storage, processing, integration, preparation, or advanced analytics then Tableau’s Technology Partner program may be right for you.</p>
                </div>
            </div>
            <p><strong>Apply to Technology Program Download Details</strong></p> --}}
            <div class="text-center">
                <a href="{{ asset('partner') }}">
                    <button class="btn btn-primary">Become a Partner</button>
                </a>
            </div>
        </div>
    </div>

    @include ('testyourself.partials.footer')
@stop
