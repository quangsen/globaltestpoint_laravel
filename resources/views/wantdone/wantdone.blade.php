@extends('master')

@section('content')
    @include('wantdone.partials.header')

    @yield ('content_wantdone')

    @include ('wantdone.partials.footer')
@endsection
