@extends ('testyourself')

@section('title')
	Test Your Self - Exam
@stop

@section('content')
<div class="container-fluid main_content_inner_page">
    <div class="container page-content">
        <div class="page-header">
            <h1>Hello , here is your score</h1>
        </div>
        <div class="row">
            <div class="col-sm-8 center">
                <h4>Thank you for taking the test.</h4>
                <div class="padding10">
                    <a href="" class="btn btn-primary"></i> Save as PDF</a> <a href="" class="btn btn-primary" target="_blank"></i> Print</a>
                </div>
                <table class="table table-bordered table-condensed table-datatable table-hover">
                    <tr>
                        <td style="text-align: left;" width="40%">Subject:</td>
                        <td style="text-align: left;" width="50%">
                            IT Job
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Exam Time:</td>
                        <td style="text-align: left;">
                            3:12
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Total Question:</td>
                        <td style="text-align: left;">
                            10
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Correct Answers:</td>
                        <td style="text-align: left;">
                            10
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Time Taken:</td>
                        <td style="text-align: left;">
                            12
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Score%:</td>
                        <td style="text-align: left;">
                            10
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-12 center">
                <h4 class="review_test_result btn btn-primary" style="cursor: pointer;"><a>Review your test</a></h4>
                <table class="table table-bordered table-condensed table-datatable table-hover table-responsive result_click_table">
                        <tr>
                            <td style="text-align: left;" width="100%"><strong>Question #1</strong></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" width="100%">
                                <img src="{!! asset('assets/images/testyourself/blank-image.png') !!}" alt=""  style=" max-width: 700px;" >
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" width="100%" class="warning">
                                <em>Question Not attempted</em>
                                <br>
                                <strong>Correct Answer is</strong>
                                <br>
                                <img src="" alt=""  style=" max-width: 700px;" >
                            </td>
                            <td style="text-align: left;" width="100%" class="success">
                                <em>Your answer is correct.</em>
                                <br>
                                <img src="" alt=""  style=" max-width: 700px;" >
                            </td>
                            <td style="text-align: left;" width="100%" class="danger">
                                <table style="width: 100%">
                                    <tr>
                                        <th style="width: 50%">Your Answer</th>
                                        <th style="width: 50%">Correct Answer</th>
                                    </tr>
                                    <tr>
                                        <td style="width: 50%">
                                        <img src="{!! asset('assets/images/testyourself/blank-image.png') !!}" alt=""  style=" max-width: 700px;" >
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" width="100%"><strong>Solution</strong></td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" width="100%">
                            <img src="{!! asset('assets/images/testyourself/blank-image.png') !!}" alt=""  style=" max-width: 700px;" >
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 5px;" width="100%">&nbsp;</td>
                        </tr>
                </table>
            </div>
            <div class="col-sm-12 center">
                <div class="no_login_review">
                     <div class="col-md-12 text-center">
                        <div class="alert alert-danger">
                            <strong>Opssss!!</strong> You must register/sign-up to be able to see your performance.
                        </div>
                    
                        </div>
                        <div class="login_register">
                            <p>Registered User: <a href="#">Login here</a></p>
                            <p>New User: <a href="#">Register/Sign-up-here</a></p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop