@extends ('testyourself')

@section('title')
	Test Your Self - Exam
@stop

@section('content')
<div class="container-fluid main_content_inner_page">
    <div class="container page-content">

        <div class="page-header">
            <h1 class="fix-h1">Start Test</h1>
        </div>
        <div class="padding10 clearfix"></div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-sm-6 center">
                <div class="well well-lg">
                    <div id="loginForm">                         
                    <form action="{!! url('do-you-want-done/exam') !!}" name="form1" id="form1" method="get" class="form-horizontal">
                            <input type="hidden" name="mode" value="start">
                            <input type="hidden" name="eid" value="43">
                            <fieldset>
                                <div class="form-group col-12 center">
                                    <div class="padding10"></div>
                                    <div class="help-block" style="color: #000;">Subject: <strong>Subject1</strong></div>
                                    <div class="help-block" style="color: #000;">Exam Time: <strong>10 minutes</strong></div>
                                    <button class="btn btn-success loginbutton" type="submit">Start Test </button>
                                </div>
                            </fieldset>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="padding30"></div>
        <div class="clearfix"></div>

    </div>
</div>
@stop
