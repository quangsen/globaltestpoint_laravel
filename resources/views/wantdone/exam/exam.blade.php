@extends ('testyourself')

@section('title')
	Test Your Self - Exam
@stop

@section('content')
<div class="container-fluid main_content_inner_page">
    <div class="container page-content">

        <div class="page-header fix-h1">
            <h1><a class="bk fix-h1" href=""><span class="back-home"> Back to home page </a></span>  </h1>
        </div>


        <div class="col-sm-6" style="overflow: hidden;height: 70px;" >
            <div class="blue_box1 tol-attmpt">
                Total Attempted: <span id="tot_atm">-</span>
            </div>
       </div>
         <div class="col-sm-6 pull-right" style="overflow: hidden;height: 70px;" id="foo"></div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-lg-12">
              <div class="well well-lg">
                <div id="loginForm">
                  <form class="form-horizontal" method="get" id="form1" name="form1" action="{!! url('/testyourself/exam-result') !!}">
                    <input type="hidden" value="end" name="mode">
                    <input type="hidden" value="" name="eid">
                    <input type="hidden" value="" name="st_exams_time">
                    <input type="hidden" value="" name="etime">
                    <fieldset>
                      <div class="form-group">
                        <div class="margin10"></div>
                        <div id="main_zone"></div>
                        <div class="padding10"></div>
                        <div class="col-sm-12 center">
                            <table style="width: 100%;" border="1" class="center">
                                <tr>
                                    <td style="text-align: right;width: 50%;padding-right: 10px;">
                                        <div id="prev-next-button">
                                            <button disabled="" type="button" class="btn btn-info">PREVIOUS</button>

                                            <button disabled="" type="button" class="btn btn-info">NEXT</button>
                                        </div>
                                    </td>
                                    <td style="text-align: left;width: 50%;"><button class="btn btn-primary center" type="button" > Submit Test</button></td>
                                </tr>
                            </table>
                        <div class="help-block center" style="color: #000;">Do not go to any other page while taking exams. Your data may be lost.</div>
                        </div>

                      </div>
                    </fieldset>
                  </form>
                </div>

              </div>
            </div>
        </div>

    </div>
</div>
@stop