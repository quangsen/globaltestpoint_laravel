@extends ('testyourself')

@section('title')
	Test Your Self - Exam
@stop

@section('content')
<div class="container-fluid main_content_inner_page">
    <div class="container page-content">
        <div class="page-header">
            <h1>
                Category: GRE
            </h1>
        </div>
        <table class="table table-bordered table-condensed table-responsive table-responsive">
            <tbody>
                <tr>
                    <th>
                        Exam subject
                    </th>
                    <th style="text-align: center;">
                        Duration
                    </th>
                    <th style="width: 120px;text-align: center;">
                        Action
                    </th>
                </tr>
                <tr>
                    <td>
                        Subject1
                    </td>
                    <td style="text-align: center;">
                        10 mins
                    </td>
                    <td style="text-align: center;">
                        <a class="btn btn-sm btn-primary" href="{!! url('/do-you-want-done/exam-start') !!}">
                            Take exam
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        Subject2
                    </td>
                    <td style="text-align: center;">
                        10 mins
                    </td>
                    <td style="text-align: center;">
                        <a class="btn btn-sm btn-primary" href="{!! url('/do-you-want-done/exam-start') !!}">
                            Take exam
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        Test
                    </td>
                    <td style="text-align: center;">
                        5 mins
                    </td>
                    <td style="text-align: center;">
                        <a class="btn btn-sm btn-primary" href="{!! url('/do-you-want-done/exam-start') !!}">
                            Take exam
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@stop