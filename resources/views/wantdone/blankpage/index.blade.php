@extends ('wantdone.wantdone') 
@section('title') 
    @if(!empty($post[0]->name))
        {{ $post[0]->name }}
    @else
        What do you want done ?
    @endif
@stop 
@section('content_wantdone')
<div id="blankpage">
    @if(isset($post))
        @foreach($post as $key=>$val)
        <div class="container">
            <div class="mgr-bottom15" style="">
                <div class="col-sm-12 col-md-12 images">
                    @if(!empty($val['image_cover']))
                    <div class="banner" style="max-height: 150px;" >
                        <img alt="image cover" src="{{{ url($val['image_cover']) }}}" width="100%" style="max-height: 150px">
                        </img>
                    </div>
                    @endif
                </div>
            </div>
            <div class="row mgr-bottom15 mgr-top35">
                <div class="col-sm-12 col-md-3">
                    <div class="link_left" style="">
                        @if(!empty($val['leftlink']))
                        <ul>
                            @foreach( $val['leftlink'] as $keyleft => $left)
                            @if(!empty($left))
                            <li>
                                <a href="{{{ $left }}}">
                                    <i aria-hidden="true" class="fa fa-link">
                                    </i>
                                    {{{ $left }}}
                                </a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12 col-md-9">
                    <div class="row">
                        <div class="col-sm-12 col-md-7 images" style="">
                            @if(!empty($val['image']))
                            <div class="photograp" style="">
                                <img alt="" src="{{{ url($val['image']) }}}" width="100%">
                                </img>
                            </div>
                            @endif
                        </div>
                        <div class="col-sm-12 col-md-5" style="">
                            <div class="link_right" style="">
                            @if(!empty($val['rightlink']))
                                <ul>
                                    @foreach( $val['rightlink'] as $keyright => $right)
                                    @if(!empty($right))
                                    <li>
                                        <a href="{{{ $right }}}">
                                            <i aria-hidden="true" class="fa fa-link">
                                            </i>
                                            {{{ $right }}}
                                        </a>
                                    </li>
                                    @endif
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="row" style="">
                        @if(!empty($val['content']))
                        <div class="introduction" style="">
                            <h3 class="introduction_title" style="">
                                Introduction
                            </h3>
                            <p>
                                {!! $val['content'] !!}
                            </p>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 sublink">
                    @if(!empty($val['sublink']))
                    <ul style="margin-top: 10px">
                        @foreach( $val['sublink'] as $keysub => $sub)
                        @if(!empty($sub))
                        <li style="display: inline;">
                            <a href="{{{ $sub }}}">
                                <i aria-hidden="true" class="fa fa-link">
                                </i>
                                {{{ $sub }}}
                            </a>
                        </li>
                        @endif
                        @endforeach
                    </ul>
                    @endif
                </div>
                <div class="col-sm-12 text-center text-uppercase forum">
                    <a href="{{ url('forum') }}">CLICK TO FORUM</a>
                </div>
            </div>
        </div>
        @endforeach
    @endif
</div>
@stop
