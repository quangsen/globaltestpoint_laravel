@extends ('testyourself.testyourself') 
@section('title')
    FAQ
@stop
@section('content_testyourself')
<div class="row">
	<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 pull-left left">
		<div class = "panel-group" id = "accordion">
			@foreach ($faqs as $faq)
			    <div class = "panel panel-default">
			      <div class = "panel-heading">
			         <h4 class = "panel-title">
			            <a data-toggle = "collapse" data-parent = "#accordion" href = "#collapse{{ $faq['id'] }}">
			               <i class="fa fa-plus-circle" aria-hidden="true"></i>
			               {{ $faq['question'] }}
			            </a> 
			         </h4>
			      </div>
			        <div id = "collapse{{ $faq['id'] }}" class = "panel-collapse collapse">
			            <div class = "panel-body">
				            {!! $faq['answer'] !!}
			            </div>
			        </div>
			    </div>
		    @endforeach
		</div>
	</div>
</div>
@stop