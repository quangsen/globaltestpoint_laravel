<div class="footer_test_yourself">
    <div class="container_site">
        <div class="col-md-12 col-xs-12">
            <div class="container-fluid">
                @if (!Route::is('testyourself.index'))
                <h4 class="hits"><a href="javascript:;">{{ (Session::has('online')) ? Session::get('online') : '0' }} hits</a></h4>
                @endif
                <div class="footer_icon">
                    <div class="row bottom_contact_us_section">
                        <div class="container">
                        <?php $optionsite = Session::get('optionsite') ?>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <figure>
                                        <a href="{{ url('contact-us') }}"><img src="{!! asset('assets/frontend/images/contact_ico.png') !!}" alt="contact"></a>
                                    </figure>
                                    <a href="#">{{ (!empty($optionsite->email) ? $optionsite->email : 'contact@demo.org') }}</a>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <figure>
                                        <img src="{!! asset('assets/frontend/images/mob_ico.png') !!}" alt="mobile">
                                    </figure>
                                    Telephone - <a href="#">{{ (!empty($optionsite->phone) ? $optionsite->phone : '+00 000 000 000') }}</a>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <figure>
                                        <img src="{!! asset('assets/frontend/images/addr_ico.png') !!}" alt="address">
                                    </figure>
                                    {{ (!empty($optionsite->address) ? $optionsite->address : 'Address company') }}
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <figure>
                                        <img src="{!! asset('assets/frontend/images/fax_ico.png') !!}" alt="fax">
                                    </figure>
                                    FAX: {{ (!empty($optionsite->fax) ? $optionsite->fax : '+00 000 000 000') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_test">
                <div class="col-md-6 col-sm-6 col-xs-12 footer_left">
                    <p>© <a href="">www.globaltestpoint.com </a>All Rights Reserved.</p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 footer_right">
                    <p>Stay connect with <a href="{{ (!empty($optionsite->facebook) ? $optionsite->facebook : 'http://www.facebook.com') }}"><i class="fa fa-facebook-official icon_fb" aria-hidden="true"></i></a> <a href="{{ (!empty($optionsite->twitter) ? $optionsite->twitter : 'http://www.twitter.com') }}"> <i class="fa fa-twitter icon_twitter" aria-hidden="true"></i></a></p>
                </div>
            </div>
        </div>
    </div>
</div>