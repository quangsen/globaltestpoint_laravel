@extends ('testyourself.testyourself') 
@section('title') 
    Blank - Testyourself
@stop 
@section('content_testyourself')
<div id="blankpage">
    @if(isset($post))
        @foreach($post as $key=>$val)
        <div class="container_site post">
            @if(!empty($val['image_cover']))
            <div class="image-cover">
                <div class="col-sm-12 col-md-12 images">
                    <div class="banner">
                        <img alt="image cover" src="{{{ url($val['image_cover']) }}}">
                        </img>
                    </div>
                </div>
            </div>
            @endif
            <div class="row left-link-row">
                @if(empty($val['leftlink']))
                    <div class="col-sm-12">
                        <div class="row left-link-image-row">
                            <div class="col-sm-12 col-md-7 left-link-image">
                                @if(!empty($val['image']))
                                    <div class="images-photograp">
                                        <div class="photograp">
                                            <img src="{{{ url($val['image']) }}}" width="100%">
                                            </img>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-sm-12 col-md-5 right-link">
                                @if(!empty($val['rightlink']))
                                    <div class="link_right">
                                        {!! $val['rightlink'] !!}
                                        {{-- <ul>
                                            @foreach( $val['rightlink'] as $keyright => $right)
                                                @if(!empty($right))
                                                <li>
                                                    <a href="{{{ $right }}}">
                                                        <i aria-hidden="true" class="fa fa-link">
                                                        </i>
                                                        {{{ $right }}}
                                                    </a>
                                                </li>
                                                @endif
                                            @endforeach
                                        </ul> --}}
                                    </div>
                                @endif
                            </div>
                            @if(!empty($val['content']))
                                <div class="col-sm-12 col-md-12 only-introduction-content">
                                    <div class="introduction">{!! $val['content'] !!}</div>
                                </div>
                            @endif
                        </div>
                    </div>
                @else
                    <div class="col-sm-12 col-md-3 left-link">
                        <div class="link_left">
                            {!! $val['leftlink'] !!}
                            {{-- <ul>
                                @foreach( $val['leftlink'] as $keyleft => $left)
                                @if(!empty($left))
                                <li>
                                    <a href="{{{ $left }}}">
                                        <i aria-hidden="true" class="fa fa-link">
                                        </i>
                                        {{{ $left }}}
                                    </a>
                                </li>
                                @endif
                                @endforeach
                            </ul> --}}
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9 right-link">
                        <div class="row">
                            @if(empty($val['rightlink']))
                                @if(!empty($val['image']))
                                <div class="col-sm-12 images-photograp">
                                    <div class="photograp">
                                        <img src="{{{ url($val['image']) }}}" width="100%">
                                        </img>
                                    </div>
                                </div>
                                @endif
                            @else
                                @if(!empty($val['image']))
                                    <div class="col-sm-12 col-md-7 images-photograp">
                                        <div class="photograp">
                                            <img src="{{{ url($val['image']) }}}" width="100%">
                                            </img>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-5 sub-right-link">
                                        <div class="link_right">
                                            {!! $val['rightlink'] !!}
                                            {{-- <ul>
                                                @foreach( $val['rightlink'] as $keyright => $right)
                                                @if(!empty($right))
                                                <li>
                                                    <a href="{{{ $right }}}">
                                                        <i aria-hidden="true" class="fa fa-link">
                                                        </i>
                                                        {{{ $right }}}
                                                    </a>
                                                </li>
                                                @endif
                                                @endforeach
                                            </ul> --}}
                                        </div>
                                    </div>
                                @else
                                    <div class="col-sm-12 right-link">
                                        <div class="link_right">
                                            {!! $val['rightlink'] !!}
                                            {{-- <ul>
                                                @foreach( $val['rightlink'] as $keyright => $right)
                                                @if(!empty($right))
                                                <li>
                                                    <a href="{{{ $right }}}">
                                                        <i aria-hidden="true" class="fa fa-link">
                                                        </i>
                                                        {{{ $right }}}
                                                    </a>
                                                </li>
                                                @endif
                                                @endforeach
                                            </ul> --}}
                                        </div>
                                    </div>
                                @endif
                            @endif
                            @if(!empty($val['content']))
                                <div class="col-sm-12 col-md-12 introduction-content">
                                    <div class="introduction">{!! $val['content'] !!}</div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
            </div>
            @if(!empty($val['sublink']))
            <div class="row sub-link-row">
                <div class="col-sm-12 col-md-12 sub-link">
                    <div class="sublink">
                        {!! $val['sublink'] !!}
                        {{-- <ul style="margin-top: 10px">
                            @foreach( $val['sublink'] as $keysub => $sub)
                                @if(!empty($sub))
                                <li style="display: inline;">
                                    <a href="{{{ $sub }}}">
                                        <i aria-hidden="true" class="fa fa-link">
                                        </i>
                                        {{{ $sub }}}
                                    </a>
                                </li>
                                @endif
                            @endforeach
                        </ul> --}}
                    </div>
                </div>
            </div>
            @endif
        </div>
        @endforeach
    @endif
    <br>
    <br>
</div>
@stop
