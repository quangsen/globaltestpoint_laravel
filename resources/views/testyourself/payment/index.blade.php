@extends ('testyourself.testyourself') @section('title') Payment online - Testyourself @stop @section('content_testyourself')
<div class="container_site payment-container">
    <div class="container">
        <div class="col-md-12 col-sm-12">
            <div class="form-group">
                <h3>
                    <u>
                        <i aria-hidden="true" class="fa fa-credit-card">
                        </i>
                        Payment Page for Globaltestpoint.com!
                    </u>
                </h3>
            </div>
        </div>
    </div>
    <div class="centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Payment Card
                        <small>
                            It's easy!
                        </small>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="stripe-errors hide">
                        <div class="alert-payment">
                            <span class="payment-errors">
                            </span>
                        </div>
                    </div>
                    @if(Auth::user())
                    <?php $user = Auth::user();?>
                    @if(!empty($customer_info))
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>
                                    Name
                                </td>
                                <td>
                                    {{ (!empty($user->last_name)) ? $user->last_name : $user->first_name }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Email
                                </td>
                                <td>
                                    {{ $customer_info->email }}
                                </td>
                            </tr>
                            <tr>
                                @if((empty($customer_info->sources['data'][0]->last4)))
                                    <td colspan="2">
                                        <p class="text-danger">An error occurred while making a payment</p>
                                    </td>
                                @else
                                    <td>
                                        Last Four
                                    </td>
                                    <td>
                                        {{ $customer_info->sources['data'][0]->last4 }}
                                    </td>
                                @endif
                            </tr>
                            @if(!empty($totalprice))
                                <tr>
                                    <td>
                                        Amount:
                                    </td>
                                    <td>
                                        {{ $totalprice }}{!! getenv('STRIPE_CURRENCY_SYMBOL') !!}
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td colspan="2">
                                    <form action="{{ (isset($checkmulti) && $checkmulti == 1) ? url('testyourself/payment-list-documents') : '' }}" id="multipayment" method="POST" role="form">
                                        <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                        <input type="hidden" name="backlink" value="{{ URL::previous() }}">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#processpayment_1">Submit Payment</button>
                                        {{-- <input id="paymentsubmit" class="btn btn-info btn-block" type="submit" name="payment" value="Submit Payment"> --}}
                                        <div class="modal fade" id="processpayment_1" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content center">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Confirm Payment !</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h4 class="custom-alert alert-warning">
                                                            Message: {{ (!empty($totalprice)) ? $totalprice : 0 }}{!! getenv('STRIPE_CURRENCY_SYMBOL') !!} will be deducted from your account
                                                        </h4>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-primary" type="submit">
                                                            Continue
                                                        </button>
                                                        <a href="{{ (!empty($backlink)) ? $backlink : '' }}">
                                                            <button class="btn btn-default" type="button">
                                                                Back
                                                            </button>
                                                        </a>
                                                        <button class="btn btn-default" data-dismiss="modal" type="button">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                    <a href="javascript:void()" class="clickhereupdate"><u>Click here to Update your card</u></a>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            $('#formupdate').css('display','none');
                            $('.clickhereupdate').click(function(){
                                $('#formupdate').css('display','block');
                            })
                        });
                    </script>
                    <hr>
                    @endif
                    <div id="formupdate">
                        <form action="{{ (isset($checkmulti) && $checkmulti == 1) ? url('testyourself/payment-list-documents') : '' }}" id="payment-form" method="POST" role="form">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <input class="form-control input-sm" data-stripe="number" id="first_name" name="" placeholder="Card number" required="" size="20" type="text">
                                        </input>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input class="form-control input-sm" data-stripe="cvc" maxlength="4" placeholder="CVC" required="" size="4" type="text">
                                </input>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <select class="form-control input-sm" data-stripe="exp-month" required="">
                                            @for($i = 1; $i <= 12; $i++)
                                            @if ($i < 10)
                                            <option value="{{ $i }}">
                                                0{{ $i }}
                                            </option>
                                            @else
                                            <option value="{{ $i }}">
                                                {{ $i }}
                                            </option>
                                            @endif
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <select class="form-control input-sm" data-stripe="exp-year" required="">
                                            @for($i = 2016; $i <= 2100; $i++)
                                            <option value="{{ $i }}">
                                                {{ $i }}
                                            </option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <label>Choose currency:</label>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="switcher-currency">
                                        <div class="form-group">
                                              <select class="form-control" id="select-currency">
                                                <option value="usd" selected="">USD</option>
                                                <option value="gbp">GBP</option>
                                                <option value="eur">EURO</option>
                                                <option value="ngn">NGN</option>
                                                <option value="zar">ZAR</option>
                                              </select>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            @endif
                                <input type="hidden" name="backlink" value="{{ URL::previous() }}">
                                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                <button type="button" class="btn btn-primary submit" data-toggle="modal" data-target="#processpayment_2">{{ (!empty($customer_info)) ? "Update Card" : "Submit Payment" }}</button>
                            <div class="modal fade" id="processpayment_2" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Confirm Payment !</h4>
                                        </div>
                                        <div class="modal-body center">
                                            <h4 class="custom-alert alert-warning">
                                                Message: {{ (!empty($totalprice)) ? $totalprice : 0 }}{!! getenv('STRIPE_CURRENCY_SYMBOL') !!} will be deducted from your account
                                            </h4>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-primary" type="submit">
                                                Continue
                                            </button>
                                            <a href="{{ (!empty($backlink)) ? $backlink : '' }}">
                                                <button class="btn btn-default" type="button">
                                                    Back
                                                </button>
                                            </a>
                                            <button class="btn btn-default" data-dismiss="modal" type="button">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop @section('script_stripe')
<script src="https://js.stripe.com/v2/" type="text/javascript">
</script>
<script src="{{ asset('assets/js/stripe.js') }}" type="text/javascript">
</script>
<script type="text/javascript">
    $(document).ready(function(){
        // $("#select-currency").change(function(){
        //     $.ajax({
        //         url: "{{ route('testyourself.payment.currency') }}",
        //         type: 'GET',
        //         data: {
        //             from: 'USD',
        //             to: 'VND'
        //         },
        //         success: function (response) {
        //             console.log(response);
        //         }
        //     });
        // });
    });
</script>
@stop
