@extends ('testyourself.testyourself') @section('title') Download Exam - Testyourself @stop @section('content_testyourself')
<div id="frontend_document" class="exam-list">
    <div class="container_site">
        <div class="search-document text-right">
            <form class="search-form form-inline col-sm-12" action="" method="GET">
                <div class="form-group">
                    <label class="sr-only" for="search">Search by exam</label>
                    <div class="input-group">
                        <input type="text" name="keyword" class="form-control" id="search" placeholder="Search by exam">
                        <div class="input-group-addon">
                            <button type="submit" class="submit-search">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="list-document">
            @if(isset($exams) && (!empty($exams))) @foreach($exams['item'] as $key => $exam)
            <div class="col-xs-12 col-sm-4 col-md-3 frame-container">
                <div class=" col-md-12 child-frame">
                    <div class="col-md-12 info-doc info-exam center">
                        <div class="col-md-12 name-doc name-exam">
                            <p>
                                {{ $exam['name'] }}
                            </p>
                        </div>
                        @if(Auth::user())
                        <div class="col-md-12 exam-button">
                            @if($exam['paid'] == 1 || $exam['price'] == 0)
                            <a href="javascript:;">
                                <button class="btn btn-primary" type="button" disabled>
                                    <i aria-hidden="true" class="glyphicon glyphicon-ok">
                                    </i> Taken Exam
                                </button>
                            </a>
                            @else
                            <a href="{{ url('testyourself/payment/exam-'.$exam['id']) }}">
                                <button class="btn btn-danger" type="button">
                                    <i aria-hidden="true" class="fa fa-download">
                                    </i> Take exam
                                    <i aria-hidden="true" class="fa fa-lock">
                                    </i> {{ $exam['price'] }}$
                                </button>
                            </a>
                            @endif
                        </div>
                        @else
                        <div class="col-md-12">
                            <a href="{{ url('login') }}">
                                <button class="btn btn-default" type="button">
                                    <i aria-hidden="true" class="fa fa-cogs">
                                    </i> Login/ Register
                                </button>
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach @endif
        </div>
        <div class="pagination pull-right">{{ $exams->links() }}</div>
    </div>
</div>
@stop
