@extends ('testyourself.testyourself') @section('title') Exam list @stop @section('content_testyourself')
<div class="container-fluid main_content_inner_page exam-list-container">
    @if ($category == null)
    <div class="container page-content">
        <div class="page-header">
            <h3 class="error">Don't exist this category</h3>
        </div>
    </div>
    @else
    <div class="container page-content">
        <div class="page-header">
            <h3>
                Category: {!! $category['name'] !!}
            </h3>
            <div class="row introduction_book">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    @if(!empty($introduction_variousexam))
                        {!! $introduction_variousexam !!}
                    @endif
                </div>
            </div>
        </div>
        <table class="exam-table table table-bordered table-condensed table-responsive table-responsive">
            <tbody>
                <tr>
                    <th class="col-xs-3">Exam subject</th>
                    <th class="text-center col-xs-2">Duration</th>
                    <th class="text-center col-xs-3">Price</th>
                    <th class="text-center col-xs-4">Action</th>
                </tr>
                @if(isset($exams)) @foreach($exams as $exam)
                <tr>
                    <td>{!! $exam->name !!}</td>
                    <td class="text-center">{!! $exam->duration !!} mins</td>
                    @if(!empty(Auth::user()))
                        @if((Auth::user()->email_verified != 1) || (Auth::user()->email_verified != '1'))
                            <td class="text-center">Your account has not been activated</td>
                        @else
                            <td class="text-center">{!! $exam->price !!} $</td>
                        @endif
                    @else
                        <td class="text-center">
                            <form action="{{ url('testyourself/saveFullURL') }}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="fullUrl" value="{{ Request::fullUrl() }}">
                                <input type="hidden" name="redirect" value="{{ url('login') }}">
                                <button class="btn btn-default" type="submit"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Login/Register</button>
                            </form>
                        </td>
                    @endif
                    <td class="take-test text-center" >
                            <p class="tooltip-mobile">Accept the terms and policy to continue. Check the bottom page!</p>
                            <a class="a-tag-exam" data-toggle="tooltip" data-placement="top" title="Accept the terms and policy to continue. Check the bottom page!"  id="take-test-{{ $exam->id }}" stt="{{ $exam->id }}" href="{!! URL::route('testyourself.exam.examStart', $exam->slug) !!}">
                                <button type="button" class="btn btn-sm btn-primary btn-exam-test">
                                    Take test
                                </button>
                            </a>
                    </td>
                </tr>
                @endforeach @endif
            </tbody>
        </table>
        <div class="back">
            <a href="{!! url('testyourself') !!}" class="btn btn-primary">
                <i class="glyphicon glyphicon-backward"></i> Back
            </a>
        </div>
        {{-- <div class="term-use">
            <input type="checkbox" name="examterm" value="" required="" id="examterm"> 
            I accept Globaltespoint’s
            <a href="{!! url('term-and-policy') !!}" class="">
            Terms of Use and Privacy Policy     
            </a>
        </div> --}}
    </div>
    @endif
</div>
{{-- <script type="text/javascript">
    $(document).ready(function(){
        $('.btn-exam-test').attr('disabled', true);
        $('.btn-exam-test').hover(function(){
            $('[data-toggle="tooltip"]').tooltip(); 
        });
        $("#examterm").change(function(){
            if(!$(this).prop('checked'))
            {
                $('.btn-exam-test').attr('disabled', true);
                $('.btn-exam-test').hover(function(){
                    $('[data-toggle="tooltip"]').tooltip();
                });
                $('.tooltip-mobile').show();
            }
            else
            {
                $('.btn-exam-test').attr('disabled', false);
                $('.btn-exam-test').hover(function(){
                    $('[data-toggle="tooltip"]').tooltip("destroy")
                });
                $('.tooltip-mobile').hide();
            }
        })
    });
</script> --}}
@stop