@extends ('testyourself.testyourself')

@section('title')
	Exam Start
@stop

@section('content_testyourself')
<div class="container-fluid main_content_inner_page">
    <div class="container page-content">
        <div class="page-header">
            <h3 class="fix-h1">Start Test</h3>
            @if(!empty($introduction_exams['introduction_exam']))
                {!! $introduction_exams['introduction_exam'] !!}
            @endif
        </div>
        <div class="padding10 clearfix"></div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-sm-6 center">
                <div class="well well-lg">
                    <div id="loginForm">                         
                    <form action="" name="form1" id="form1" method="post" class="form-horizontal">
                            <input type="hidden" name="mode" value="start">
                            <input type="hidden" name="eid" value="43">
                            <fieldset>
                                <div class="form-group col-12 center">
                                    <div class="padding10"></div>
                                    <div class="help-block" style="color: #000;">Subject: <strong>{!! $exam->name !!}</strong></div>
                                    <div class="help-block" style="color: #000;">Exam Time: <strong>{!! $exam->duration !!} minutes</strong></div>
                                    <a href="{!! URL::route('testyourself.getExam', $exam->slug) !!}">
                                        <button class="btn btn-success loginbutton" type="button">Start Test </button>
                                    </a>
                                    <br><br>
                                    <div class="term-use">
                                        <input type="checkbox" name="examterm" value="" required="" id="examterm"> 
                                        I accept Globaltespoint’s
                                        <a href="{!! url('term-and-policy') !!}" class="">
                                        Terms of Use and Privacy Policy     
                                        </a>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="padding30"></div>
        <div class="clearfix"></div>

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.loginbutton').attr('disabled', true);
        $("#examterm").change(function(){
            if(!$(this).prop('checked'))
            {
                $('.loginbutton').attr('disabled', true);
            }
            else
            {
                $('.loginbutton').attr('disabled', false);
            }
        })
    });
</script>
@stop
