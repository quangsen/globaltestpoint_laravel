<p>Your payment of {{$exam['price']}}$ for Exam {{$exam['name']}} on globaltestpoint was successful.</p>
<p>Kindly proceed and take your test.</p>
<p>Good luck in your test.</p>
<p>Yours,</p>
<p>Globaltestpoint Support Team.</p>
<p><a href="mailto:info@globaltestpoint.com">Info@globaltestpoint.com</a></p>
<p>Baltimore MD, USA.</p>
<p>Tel: +1 703 332 9157</p>
