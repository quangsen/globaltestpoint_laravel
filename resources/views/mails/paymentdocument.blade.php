<p>Thank you {{ $user->first_name }} for subscribing to our material. Your payment was successful and you can go ahead with your transaction.</p>
<p>Yours,</p>
<p>Globaltestpoint Support Team.</p>
<p><a href="mailto:info@globaltestpoint.com">Info@globaltestpoint.com</a></p>
<p>Baltimore MD, USA.</p>
<p>Tel: +1 703 332 9157</p>