@extends ('testyourself.testyourself') 
@section('title') 
    Globaltestpoint Term and Policy
@stop 
@section('content_testyourself')
	<div class="container_site">
        <div class="col-sm-12 termpolicy">
        	<div class="col-sm-12">
        		<h3><u>Globaltestpoint Terms of Use and Privacy Policy</u></h3>
        	</div>
            <div class="col-sm-12">
            	{!! (!empty($img_logo['termpolicy']) ? $img_logo['termpolicy'] : '') !!}
            </div>
        </div>
    </div>
@stop