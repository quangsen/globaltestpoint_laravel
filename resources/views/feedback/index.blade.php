@extends ('testyourself.testyourself') 
@section('title') 
    Contact Us
@stop 
@section('content_testyourself')
    <div class="container_site">
        <div class="col-sm-12 contact-us">
            <div class="col-md-9 col-md-offset-1 col-sm-12">
                <form action="" method="post" role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name">Your Fullname : </label>
                        <div class="col-sm-9">
                            <input type="text" required="" value="" placeholder="First &amp; Last Name" name="fullname" id="fullname" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="email">Email Address : </label>
                        <div class="col-sm-9">
                            <input type="email" required="" value="" placeholder="example@domain.com" name="email" id="email" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="message">Email Subject : </label>
                        <div class="col-sm-9">
                            <input type="text" required="" value="" placeholder="Email Subject" name="subject" id="subject" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="message">Your Message:</label>
                        <div class="col-sm-9">
                            <textarea required="" placeholder="Your Message" name="message" rows="4" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="human">Prove You're Not A Robot :</label>
                        <div class="col-sm-9">
                            <div class="container_fluider_sigup_recapcha">
                                <div class="g-recaptcha" data-sitekey="{{ getenv('GOOGLE_CAPTCHA') }}"></div>
                            </div>
                        </div>
                        <div class="col-sm-10 col-sm-offset-2 error-capcha">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <input name="_token" type="hidden" value="{{ csrf_token() }}">
                            <input type="submit" required="" class="btn btn-primary" value="Send" name="submit" id="submit">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <!-- Will be used to display an alert to the user-->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
