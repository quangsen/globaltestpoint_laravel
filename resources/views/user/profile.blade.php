@extends ('testyourself.testyourself') 
@section('title') Testyourself 
@stop 
@section('content_testyourself') 
@if(isset($user))

<div class="container_site">
    <div class="col-md-12">
        @include('blocks.error')
        <div id="testyourself" class="sign_up_page">
            <div id="signup">
                <div class="panel panel-success ">
                    <div class="panel-body panel-heading">
                        Profile: <a href="{!! url('user/profile') !!}">View</a> | <a href="{!! url('user/edit/profile') !!}">Edit Profile</a> | <a href="{!! url('user/edit/changePassword') !!}">Edit Password</a>
                    </div>
                    <div class="panel-footer bg_white">
                        <div class="container-fluid main_content_inner_page container_fluider_sigup">
                            <div class="container container_sign_up">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Full Name</label>
                                        <input type="text" disabled="disabled" class="form-control" placeholder="First Name" value="{!! $user->first_name !!}">
                                    </div>
                                    {{-- <div class="form-group">
                                        <label for="exampleInputPassword1">Middle Name</label>
                                        <input disabled="disabled" type="text" class="form-control" placeholder="Middle Name" value="{!! $user->middle_name !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Last Name</label>
                                        <input disabled="disabled" type="text" class="form-control" placeholder="Last Name" value="{!! $user->last_name !!}">
                                    </div> --}}
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Email</label>
                                        <input disabled="disabled" type="email" class="form-control" placeholder="Email" value="{!! $user->email !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Telephone</label>
                                        <input disabled="disabled" type="text" class="form-control" placeholder="Telephone" value="{!! $user->mobile !!}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">BirthDay</label>
                                        <input disabled="disabled" type="text" class="form-control" placeholder="BirthDay" value="{!! $date !!}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Gender</label>
                                        @if(isset($genders)) @foreach($genders as $key => $gender)
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-11 col-md-offset-1">
                                                <label class="lb_radio label_gender_register">{!! $gender !!}</label>
                                                <input @if($user->gender == $key) {!! "checked" !!} @endif class="" name="gender" type="radio" value="1" tabindex="9" disabled="disable">
                                            </div>
                                        </div>
                                        @endforeach @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Location</label>
                                        <select class="form-control" id="ulocation" name="ulocation" tabindex="6" disabled="disable">
                                            <option>-- Select --</option>
                                            @if(isset($locations)) @foreach($locations as $key=>$localtion)
                                            <option value="" @if($user->country == $localtion['id']) {!! "selected='selected'" !!} @endif> {!! $localtion['name'] !!}
                                            </option>
                                            @endforeach @endif
                                        </select>
                                    </div>
                                    {{-- <div class="form-group">
                                        <label for="exampleInputPassword1">User Type</label>
                                        <select class="form-control" id="ulocation" name="ulocation" tabindex="6" disabled="disable">
                                            <option>-- Select --</option>
                                            @if(isset($user_types)) @foreach($user_types as $key=>$user_type)
                                            <option value="" @if($user->user_type == $user_type) {!! "selected='selected'" !!} @endif> {!! $user_type !!}
                                            </option>
                                            @endforeach @endif
                                        </select>
                                    </div> --}}
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Current Photo</label>
                                        <img src="{!! url($user->image) !!}" alt="" class="profile_img_current">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif 
@stop
