@extends ('testyourself.testyourself') 
@section('title') 
Edit Profile 
@stop 
@section('content_testyourself') 
@if(isset($user))
<div class="container_site">
    <div class="col-md-12">
        @if(count($errors) >0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{!! $error !!}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div id="testyourself" class="sign_up_page">
            <div id="signup">
                <div class="panel panel-success ">
                    <div class="panel-body panel-heading">
                        Profile: <a href="{!! url('user/profile') !!}">View</a> |@if((Auth::user()->email_verified == 1) || (Auth::user()->email_verified == '1')) <a href="{!! url('user/edit/profile') !!}">Edit Profile</a> | <a href="{!! url('user/edit/changePassword') !!}">Edit Password</a>@endif
                    </div>
                    <div class="panel-footer bg_white">
                        <div class="container-fluid main_content_inner_page container_fluider_sigup">
                            <div class="container container_sign_up">
                                <form action="" method="post" enctype="multipart/form-data">
                                    <div class="col-md-6">
                                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Full Name <span class="required">*</span></label>
                                            <input type="text" name="fname" class="form-control" id="exampleInputEmail1" placeholder="First Name" value="{!! $user->first_name !!}">
                                        </div>
                                        {{-- <div class="form-group">
                                            <label for="exampleInputPassword1">Middle Name</label>
                                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Middle Name" value="{!! $user->middle_name !!}" name="mname">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Last Name <span class="required">*</span></label>
                                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Middle Name" value="{!! $user->last_name !!}" name="lname">
                                        </div> --}}
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Telephone</label>
                                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Telephone" value="{!! $user->mobile !!}" name="utel">
                                        </div>
                                        <label for="exampleInputPassword1">BirthDay</label>
                                        <div class="input-group form-group date" data-provide="datepicker">
                                            <input type="text" class="form-control" value="{!! $date !!}" name="udob">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Gender <span class="required">*</span></label>
                                            @if(isset($genders)) @foreach($genders as $key => $gender)
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-11 col-md-offset-1">
                                                    <label class="lb_radio label_gender_register">{!! $gender !!}</label>
                                                    <input @if($user->gender == $key) {!! "checked" !!} @endif class="" name="gender" type="radio" value="{!! $key !!}" tabindex="9" >
                                                </div>
                                            </div>
                                            @endforeach @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Location <span class="required">*</span></label>
                                            <select class="form-control" id="ulocation" name="ulocation" tabindex="6">
                                                <option value="">-- Select --</option>
                                                @if(isset($locations)) 
                                                    @foreach($locations as $key=>$localtion)
                                                    <option value="{!! $localtion['id'] !!}" @if($user->country == $localtion['id']) {!! "selected='selected'" !!} @endif> {!! $localtion['name'] !!}
                                                    </option>
                                                    @endforeach 
                                                @endif
                                            </select>
                                        </div>
                                        {{-- <div class="form-group">
                                            <label for="exampleInputPassword1">User Type <span class="required">*</span></label>
                                            <select class="form-control" id="ulocation" name="utype" tabindex="6">
                                                <option value="">-- Select --</option>
                                                @if(isset($user_types)) @foreach($user_types as $key=>$user_type)
                                                <option value="{!! $user_type !!}" @if($user->user_type == $user_type) {!! "selected='selected'" !!} @endif> {!! $user_type !!}
                                                </option>
                                                @endforeach @endif
                                            </select>
                                        </div> --}}
                                        @if((Auth::user()->email_verified != 1) || (Auth::user()->email_verified != '1'))
                                            <div class="alert alert-danger">
                                                Your account status is still pending: Check your mail to activate your account or contact the Admin to upload your's avatar
                                            </div>
                                        @else
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Current Photo</label>
                                                <img src="{!! url(Auth::user()->image) !!}" alt="" class="profile_img_current">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">New Photo</label>
                                                <input type='file' name="NewImages" class="img_new_photo">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif 
@stop
