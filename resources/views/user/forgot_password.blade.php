@extends ('testyourself.testyourself') @section('title') Forgot password @stop @section('content_testyourself')
<div class="container_site">
    <div class="col-md-12">
        <div id="testyourself" class="sign_up_page">
            <div id="signup">
                <div class="panel panel-success ">
                    <div class="panel-body panel-heading">
                        Forgot Password
                    </div>
                    <form action="{{ url('password/email') }}" method="post">
                        <div class="panel-footer bg_white">
                            <div class="container-fluid main_content_inner_page container_fluider_sigup">
                                <div class="container container_sign_up">
                                    <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">
                                        @if (session('status'))
                                        <div class="alert alert-success">
                                            {{ session('status') }}
                                        </div>
                                        @endif
                                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="exampleInputEmail1">Email</label>
                                            <input name="email" type="email" class="form-control" placeholder="Enter Email" value=""> @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span> 
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <a href="{{ url('login') }}" title="Login" class="pull-right">Back to Login</a>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
