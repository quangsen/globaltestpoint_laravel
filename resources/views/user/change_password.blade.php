@extends ('testyourself.testyourself') 
@section('title') Testyourself 
@stop 
@section('content_testyourself')

<div class="container_site">
    <div class="col-md-12">
        @include('blocks.error')
        <div id="testyourself" class="sign_up_page">
            <div id="signup">
                <div class="panel panel-success ">
                    <div class="panel-body panel-heading">
                        Profile: <a href="{!! url('user/profile') !!}">View</a> | <a href="{!! url('user/edit/profile') !!}">Edit Profile</a> | <a href="{!! url('user/edit/changePassword') !!}">Edit Password</a>
                    </div>
                    <form action="" method="post">                    
                        <div class="panel-footer bg_white">
                            <div class="container-fluid main_content_inner_page container_fluider_sigup">
                                <div class="container container_sign_up">
                                    <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">
                                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Current Password</label>
                                            <input name="current_password" type="password" class="form-control" placeholder="Current Password" value="">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">New Password</label>
                                            <input name="password" type="password" class="form-control" placeholder="New Password" value="">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Retype New Password</label>
                                            <input name="retype_password" type="password" class="form-control" placeholder="Retype New Password" value="">
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success">Update</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
