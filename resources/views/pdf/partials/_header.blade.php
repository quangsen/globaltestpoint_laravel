<table>
    <tr>
        <td>
            <h2>{{ $primary_content }}</h2>
            <p>{{ $secondary_content }}</p>
        </td>
    </tr>
</table>
