<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $title ? $title : 'Examination Result' }}</title>
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pdf/pdf.css') }}">
</head>

<body>
    <div class="main-container">
        @if (isset($examResults) && count($examResults) > 0)
        <div class="main-content">
            <h3 class="text-center">Examination Result</h3>
            <table class="table table-bordered table-condensed table-datatable table-hover">
                <thead>
                    <tr>
                        <th class="col-xs-2">User Name</th>
                        <th class="col-xs-2">Email</th>
                        <th class="col-xs-2">Test Name</th>
                        <th class="col-xs-2 text-center">Total Marks (%)</th>
                        <th class="col-xs-2">Time Taken</th>
                        <th class="col-xs-2">Exam Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($examResults as $examResult)
                    <tr>
                        <td class="col-xs-2">{{ $examResult['user']['first_name'] }} {{ $examResult['user']['last_name'] }}</td>
                        <td class="col-xs-2">{{ $examResult['user']['email'] }}</td>
                        <td class="col-xs-2">{{ $examResult['data']['exam']['name'] }}</td>
                        <td class="col-xs-2 text-center">{{ number_format($examResult['data']['score'], 2) }}%</td>
                        <td class="col-xs-2">{{ $examResult['timeTaken'] }}</td>
                        <td class="col-xs-2">{{ $examResult['examDate'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th class="col-xs-2">User Name</th>
                        <th class="col-xs-2">Email ID</th>
                        <th class="col-xs-2">Test Name</th>
                        <th class="col-xs-2 text-center">Total Marks (%)</th>
                        <th class="col-xs-2">Time Taken</th>
                        <th class="col-xs-2">Exam Date</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        @else
        <table class="table table-bordered">
            <tr>
                <td>
                    <p class="alert alert-danger">Don't exist any exam</p>
                </td>
            </tr>
        </table>
        @endif
    </div>
</body>

</html>
