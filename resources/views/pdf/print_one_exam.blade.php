<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $title ? $title : 'Examination Result' }}</title>
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pdf/pdf.css') }}">
</head>

<body>
    <div class="main-container">
        @if ($examResult != null)
        <div class="main-content">
            <h3 class="text-center">Examination Result</h3>
            <table class="table table-bordered table-condensed table-datatable table-hover">
                <tr>
                    <td class="col-xs-4">
                        <strong>Name:</strong>{{ $examResult['user']['first_name'] }} {{ $examResult['user']['last_name'] }}
                    </td>
                    <td class="col-xs-4">
                        <strong>Email:</strong> {{ $examResult['user']['email'] }}
                    </td>
                    <td class="col-xs-4">
                        <strong>Test Taken On:</strong> {{ $examResult['examDate'] }}
                    </td>
                </tr>
            </table>
            <table class="table table-bordered table-condensed table-datatable table-hover">
                <tr>
                    <td class="col-xs-4">Subject:</td>
                    <td class="col-xs-8">{{ $examResult['data']['exam']['name'] }}</td>
                </tr>
                <tr>
                    <td class="col-xs-4">Exam Time:</td>
                    <td class="col-xs-8">{{ $examResult['data']['exam']['duration'] }} {{ $examResult['data']['exam']['duration'] > 1 ? 'minutes' : 'minute' }}</td>
                </tr>
                <tr>
                    <td class="col-xs-4">Total Question:</td>
                    <td class="col-xs-8">{{ $examResult['data']['totalQuestion'] }}</td>
                </tr>
                <tr>
                    <td class="col-xs-4">Correct Answers:</td>
                    <td class="col-xs-8">{{ $examResult['data']['correctAnswers'] }}</td>
                </tr>
                <tr>
                    <td class="col-xs-4">Time Taken:</td>
                    <td class="col-xs-8">{{ $examResult['timeTaken'] }}</td>
                </tr>
                <tr>
                    <td class="col-xs-4">Score%:</td>
                    <td class="col-xs-8">{{ number_format($examResult['data']['score'], 2) }}%</td>
                </tr>
            </table>
        </div>
        @else
        <table class="table table-bordered">
            <tr>
                <td>
                    <p class="alert alert-danger">Exam not found</p>
                </td>
            </tr>
        </table>
        @endif
    </div>
</body>

</html>
