<div class="form-group">
    <label for="document"><span class="required">*</span> Question</label>
    <input type="text" class="form-control" id="question" name="question" value="{!! (isset($faq['question']) && !empty($faq['question']) ? $faq['question'] : '') !!}">
</div>
<div class="form-group">
    <span class="glyphicon glyphicon-cog"></span> Answer</div>
        <textarea class="rich_editor" name="answer" id="answer">{!! (isset($faq['answer']) && !empty($faq['answer']) ? $faq['answer'] : '') !!}</textarea>
        <script>
            ckeditor('answer');
        </script>
        <br>
        <button type="submit" class="btn btn-primary">{{ isset($buttonTitle) && $buttonTitle != null ? $buttonTitle : 'Submit' }}</button>
    </div>
</div>
