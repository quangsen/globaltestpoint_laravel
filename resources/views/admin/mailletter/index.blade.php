@extends ('admin')

@section('title')
    Globaltestpoint - Dashboard Category
@stop

@section ('content')
<div class="container option_site">
	<div class="row">
		<div class="panel panel-default">
            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> EDIT CONTENT MAIL</div>
            <div class="panel-body">
                <div class="margin10">
                	@if(Session::has('result'))
                	<div class="form-group col-sm-12 col-md-6 center">
		        		@if(Session::get('result'))
		        		<div class="alert alert-success">
		                    {{ Session::get('message') }}
		                </div>
			            @else
			            	<div class="alert alert-danger">
			                    {{ Session::get('message') }}
			                </div>
			            @endif
	        		</div>
	        		@endif
	        		<br/>
                    <div class="padding5 clearfix"></div>
                    <div id="main_mailletter">
                    	<form action="" method="post" accept-charset="utf-8" >
                    		<div class="row">
	                    		<div class="col-sm-12 col-md-12">
	                    			<div class="form-group">
	                    				<label for="">Email contact: </label>
	                    				<input type="email" class="form-control" name="email" id="email" value="{{ (isset($email['email']) ? $email['email'] : old('email')) }}" placeholder="">
	                    			</div>
	                    			<div class="form-group">
	                    				<label for="">Email Register: </label>
	                    				<textarea name="register" class="form-control rich_editor">{{ (isset($email['register']) ? $email['register'] : old('register')) }}</textarea>
	                    			</div>
	                    			<div class="form-group">
	                    				<label for="">Email reset password: </label>
	                    				<textarea name="reset_password" class="form-control rich_editor">{{ (isset($email['reset_password']) ? $email['reset_password'] : old('reset_password')) }}</textarea>
	                    			</div>
	                    			<div class="form-group">
	                    				<label for="">Email Complete Exam: </label>
	                    				<textarea name="complete_exam" class="form-control rich_editor">{{ (isset($email['complete_exam']) ? $email['complete_exam'] : old('complete_exam')) }}</textarea>
	                    			</div>
	                    			<div class="form-group">
	                    				<label for="">Email Subcribe: </label>
	                    				<textarea name="subcribe" class="form-control rich_editor">{{ (isset($email['subcribe']) ? $email['subcribe'] : old('subcribe')) }}</textarea>
	                    			</div>
	                    		</div>
	                    	</div>
	                    	<div class="row">
	                    		<div class="col-md-3">
	                    			<input name="_token" type="hidden" value="{{ csrf_token() }}">
	                    			<input type="submit" class="btn btn-primary" name="btn_option" value="Save Setting">
	                    		</div>
	                    	</div>
                    	</form>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
@stop

