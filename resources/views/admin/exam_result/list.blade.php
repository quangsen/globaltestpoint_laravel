@extends ('admin') @section('title') {{$title}} @stop @section ('content')
<div class="container school-degree-container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span><i class="glyphicon glyphicon-cog"></i> {{$tableTitle}}</span>
            </div>
            <div class="panel-body">
                <div class="margin10">
                    <div class="col-sm-12 margin-bottom-10">
                        <a href="{{ url('print-pdf/print-all-result') }}" class="btn btn-success">
                            <i class="glyphicon glyphicon-print"></i> Print all
                        </a>
                    </div>
                    <div class="col-sm-12">
                        <table id="list_exam_result" class="table table-striped table-bordered data-table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    @if(!empty($examResults) && count($examResults) > 0)
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Test Name</th>
                                    <th class="text-center">Total Marks (%)</th>
                                    <th>Time Taken</th>
                                    <th>Exam Date</th>
                                    <th>Action</th>
                                    @else
                                    <th colspan="7" class="no-record">You don't have any record</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($examResults)) @foreach ($examResults as $examResult)
                                <tr>
                                    <td>{{ $examResult['user']['first_name'] }} {{ $examResult['user']['last_name'] }}</td>
                                    <td>{{ $examResult['user']['email'] }}</td>
                                    <td>{{ $examResult['data']['exam']['name'] }}</td>
                                    <td class="text-center">{{ number_format($examResult['data']['score'], 2) }}%</td>
                                    <td>{{ $examResult['timeTaken'] }}</td>
                                    <td>{{ $examResult['examDate'] }}</td>
                                    <td>
                                        <a href="{{ url('print-pdf/print-result', $examResult['id']) }}" class="btn btn-success">
                                            <i class="glyphicon glyphicon-print"></i> Print
                                        </a>
                                    </td>
                                </tr>
                                @endforeach @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
