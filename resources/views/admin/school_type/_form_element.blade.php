<div class="form-group">
    <label for="name"><span class="required">*</span> Name</label>
    <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{ isset($school_type['name']) && $school_type['name'] != null ? $school_type['name'] : old('name') }}">
</div>
<div class="form-group">
    <label for="order">Order</label>
    <input type="text" name="order" class="form-control" id="order" placeholder="Order" value="{{ isset($school_type['order']) && $school_type['order'] != null ? $school_type['order'] : 0 }}">
</div>
<div class="form-group">
    <label for="active">Active</label>
    <input name="active" id="active" type="checkbox" value="1" {{ isset($school_type['active']) && $school_type['active'] == 1 ? 'checked' : 'checked' }}></input>
</div>
<button type="submit" class="btn btn-primary">{{ isset($buttonTitle) && $buttonTitle != null ? $buttonTitle : 'Submit' }}</button>
