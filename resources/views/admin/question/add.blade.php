@extends('admin') @section('title') Add Exams @stop @section('content') @include('_include_angular')
<div id="add_exams" ng-app="app" ng-controller="AdminExamCtrl as vm">
    <div class="container edit_users-admin add-question-container" ng-init="exam_id = {{$exam['id']}}">
        <div class="row">
            <div ng-show="vm.flashMessage" ng-class="vm.flashClass" class="error-angular alert">
                <ul>
                    <li ng-bind="vm.flashMessage"></li>
                </ul>
            </div>
            @include('include._laravel_error')
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-edit"></span> Add subject</div>
                <div class="container_test_input panel-footer container">
                    <div class="col-md-12">
                        <div class="list_Exam_in_question">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="name">Name:</label>
                                <div class="col-sm-10 col_input_bottom">
                                    <input type="text" class="form-control name_questions" disabled="disabled" id="name" placeholder="Enter name" name="name" value="{!! old('name', isset($exam) ?$exam->name : null) !!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="sex">Category:</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="sel1" name="category" disabled="disabled">
                                        <option value="0">{!! $category['name'] !!}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="address">Duration:</label>
                                <div class="col-md-10 col-sm-10 col-xs-12 input-group duration_add_exam">
                                    <input type="text" class="form-control" id="address" placeholder="Enter Duration" name="duration" value="{!! old('duration', isset($exam) ? $exam->duration : null) !!}" aria-describedby="basic-addon2" disabled="disabled">
                                    <span class="input-group-addon" id="basic-addon2">MINUTES</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="address">Questions</label>
                                <div class="col-md-10">
                                    <a href="{!! URL::route('admin.exam.lists') !!}">
                                        <button type="button" class="btn btn-warning"><i class="glyphicon glyphicon-backward"></i> Back to subjects</button>
                                    </a>
                                    <button type="button" class="btn btn-primary add_new_questions" name="add_new_questions"><i class="glyphicon glyphicon-plus-sign"></i> Add new Questions</button>
                                    <button type="button" class="btn btn-info" id="toggleBulkQuestion"><i class="glyphicon glyphicon-plus-sign"></i> Add Bulk Question</button>
                                </div>
                            </div>
                            <div class="form-group" id="bulkQuestion">
                                <label class="control-label col-sm-2" for="bulk-block"><span class="required">*</span> Attach File: </label>
                                <div class="col-md-10">
                                    <div>
                                        <form class="form-horizontal" name="qform" id="qform" action="{{ url('admin/question/addQuestionByCSVFile', $exam['id']) }}" method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="_method" value="POST">
                                            <fieldset>
                                                <div class="form-group">
                                                    <div class="col-10 col-offset-2">
                                                        <a class="btn btn-sm btn-default" href="{{ asset('uploads/questions/sampleCSV/test.csv') }}"><i class="glyphicon glyphicon-download"></i> download sample csv</a>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-6 no-padding">
                                                        <div>
                                                            <input type="file" name="csv_file" id="csv_file" class="csv_file upload-csv filestyle" onChange="load_csv(this.id, this.value)" tabindex="-1">
                                                        </div>
                                                        Add csv files only. Maximum size 2MB
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-10 col-offset-2">
                                                        <button class="btn btn-primary" type="submit">Submit</button>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form class="form-horizontal form-questions" method="post" name="vm.addQuestionForm" ng-submit="vm.addQuestion(vm.question)">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <div class="list_option_add" id="questions">
                                <div id="question" class="question">
                                    <div class="form-group">
                                        <label class="control-label col-xs-12 col-sm-3">Question</label>
                                        <div class="col-xs-12 col-sm-9">
                                            <div ng-show="vm.question.type === 1">
                                                <textarea ckeditor="" ng-model="vm.question.question" id="addQuestion"></textarea>
                                            </div>
                                            <div ng-show="vm.question.image && vm.question.type === 2">
                                                <img ng-src="@{{asset(vm.question.image)}}" class="img-responsive img-thumbnail">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-xs-12 col-sm-3">Solution</label>
                                        <div class="col-xs-12 col-sm-9">
                                            <textarea ckeditor="" ng-model="vm.question.solution"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="question" ng-show="vm.errorUploadMsg && vm.question.type === 2">
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-9 col-sm-offset-3">
                                            <div class="error" ng-bind="vm.errorUploadMsg"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="question" ng-show="vm.question.type === 2">
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-9 col-sm-offset-3">
                                            <button class="upload-image btn btn-success" type="button" dropzone model-name="vm.question.image" error-message="vm.errorUploadMsg" folder-upload="questions">Upload Image</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="question">
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-9 col-sm-offset-3">
                                            <div class="pull-left" ng-show="vm.question.type === 1">
                                                <a class="btn btn-success question-type question-tpe-image" ng-click="vm.question.type = 2">Question Image</a>
                                            </div>
                                            <div class="pull-left" ng-show="vm.question.type === 2">
                                                <a class="btn btn-success question-type question-tpe-text" ng-click="vm.question.type = 1">Question Text</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="answers col-xs-12 col-md-9 col-md-offset-3">
                                    <div class="answer" ng-repeat="answer in vm.question.answers">
                                        <div class="form-group">
                                            <div class="col-xs-12 margin-bottom-10">
                                                <div class="pull-left">
                                                    <label>Option @{{$index + 1}}</label>
                                                </div>
                                                <div class="pull-right">
                                                    <a class="btn btn-danger" ng-click="vm.removeAnswer(answer)">Remove</a>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div ng-if="answer.type != 2">
                                                    <textarea ckeditor="" ng-model="answer.answer"></textarea>
                                                    <div class="form-group col-xs-12">
                                                        <input type="checkbox" id="is-correct-@{{$index}}" ng-model="answer.is_correct">
                                                        <label for="is-correct-@{{$index}}">Choose if this answer is correct</label>
                                                    </div>
                                                </div>
                                                <div ng-if="answer.type == 2">
                                                    <input type="file" class="form-control" value="" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <a class="btn btn-default" ng-click="vm.addMoreAnswer(vm.question)">Add Answer</a>
                                    </div>
                                </div>
                                <div class="form-group text-right add_question_new">
                                    <button ng-disabled="vm.inProgress" type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel panel-info" ng-if="vm.exam.questions && vm.exam.questions.data.length > 0">
                <div class="panel-body panel-heading">
                    <span class="glyphicon glyphicon-edit"></span> List Questions
                </div>
                <div class="panel-footer">
                    <div class="">
                        <div class="panel-group" id="accordion">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <div class="panel panel-default button_list_question_manage_container" ng-repeat="question in vm.exam.questions.data">
                                <div class=" btn btn-info button_list_question_manage" my-toggle="collapse" toggle-element="#collapse@{{$index + 1}}">
                                    <h4 class="panel-title">
                                      <a><span class="glyphicon glyphicon-question-sign"></span> Question @{{$index + 1}}</a>
                                    </h4>
                                </div>
                                <div id="collapse@{{$index + 1}}" class="panel-body content_question_list_add">
                                    <div class="form-group overflow-x-hidden">
                                        <label class="control-label col-sm-3">Question Name</label>
                                        <div class="col-sm-9">
                                            <div ng-if="question.type == 1">
                                                <p ng-bind-html="question.question"></p>
                                            </div>
                                            <div ng-if="question.type == 2">
                                                <img ng-src="@{{ asset(question.question) }}" class="img-responsive img-thumbnail">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="options@{{keyAnswer + 1}}" ng-if="question.answers && question.answers.data.length > 0" ng-repeat="(keyAnswer, answer) in question.answers.data">
                                        <div class="form-group overflow-x-hidden">
                                            <label class="control-label col-sm-3">Answer @{{keyAnswer + 1}}</label>
                                            <div class="col-xs-9">
                                                <div ng-bind-html="answer.answer"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group button_list_question_submit">
                                        <div class="col-md-12">
                                            <a href="javascript:;" ng-click="vm.deleteQuestion(asset('admin/question/delete/' + question.id))" class="margin-left-10 btn btn-danger pull-right"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                                            <a ng-href="@{{ asset('admin/question/update/' + question.id) }}" class="btn btn-success pull-right"><i class="glyphicon glyphicon-edit"></i> Update</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
