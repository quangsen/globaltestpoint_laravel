@extends ('admin')

@section('title')
    Globaltestpoint - Dashboard Category
@stop

@section ('content')
<div class="container">
	<div class="row">
		<div class="panel panel-default">
            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> List video</div>
            <div class="panel-body">
                <div class="margin10">
                	<div class="form-group col-sm-6 center">
			        	@if(isset($result))
		        		   @if($result)
                            <div class="alert alert-success">
                                {{{ $message }}}
                            </div>
                            @else
                            <div class="alert alert-danger">
                                {{{ $message }}}
                            </div>
                            @endif
			            @endif
	        		</div>
	        		@include('blocks.error')
	        		<br/>
                    <div class="padding5 clearfix"></div>
                    <div class="col-sm-12">
                    	<table id="list_video" class="table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
					                <th>STT</th>
					                <th>Link</th>
					                <th>Active</th>
					                <th>Type</th>
					                <th>Page</th>
					                <th>Author</th>
					                <th>Update date</th>
					                <th>Action</th>
					            </tr>
							</thead>
							<tbody>
							@if(isset($videos))
								<?php $i = 0; ?>
								@foreach ($videos as $keyvideo => $video)
								<tr>
					                <td> {{{ ++$i }}}</td>
					                <td>{{{ $video['link'] }}}</td>
					                @if($video['active'] == 1)
					                	<td><span class="label label-success">Active</span></td>
					                @else
					                	<td><span class="label label-danger">No Active</span></td>
					                @endif
					                <td>{{{ ($video['type'] == 0) ? 'Video' : 'Flash' }}}</td>
					                <td>{{{ ($video['page'] == 0) ? 'Test Yourself' : 'You Want Done' }}}</td>
					                <td>{{{ $video['nameuser'] }}}</td>
					                <td>{{{ $video['updated_at'] }}}</td>
					                <td>
										<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit_video_{{ $video['id'] }}">Edit</button>
										<a href="javascript:void(0);" data-target="#confirm-delete_{{{ $video['id'] }}}" data-toggle="modal" data-href="{{{ url('admin/delete-category/') }}}/{{{ $video['id'] }}}">
                                            <button stt="{{{ $video['id'] }}}" type="button" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                                        </a>
                                        <div id="confirm-delete_{{{ $video['id'] }}}" class="modal fade" role="dialog">
										    <div class="modal-dialog">
											    <!-- Modal content-->
											    <div class="modal-content">
											        <div class="modal-header">
											            <button type="button" class="close" data-dismiss="modal">&times;</button>
											            <h4 class="modal-title">Are you sure to delete this video ?</h4>
											        </div>
											        <div class="modal-body">
											        	<form action="" method="post" accept-charset="utf-8" id="form_delete_{{{ $video['id'] }}}">
												            <div class="form-group center">
												            	<input type="hidden" name="id_delete_video" value="{{{ $video['id'] }}}">
												        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
												        		<input name="btn_delete" for="#form_delete_{{{ $video['id'] }}}" type="submit" class="btn btn-danger" value="OK">
												            	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
												            </div>
											        	</form>
											        </div>
											    </div>
										    </div>
										</div>		               	
					                </td>
					            </tr>
					            <div id="edit_video_{{{ $video['id'] }}}" class="modal fade" role="dialog">
								    <div class="modal-dialog">
								        <div class="modal-content">
								            <div class="modal-header">
									            <button type="button" class="close" data-dismiss="modal">&times;</button>
									            <h4 class="modal-title">Edit </h4>
									        </div>
									        <div class="modal-body">
									            <form action="" method="post" accept-charset="utf-8" id="form_{{{ $video['id'] }}}">
									            	<div class="col-sm-12">
						                                <div class="form-group">
						                                    <label>
						                                        Link youtube:
						                                    </label>
						                                    <input class="form-control" id="inp_video" name="inp_video" placeholder="" type="text" value="{{{ $video['link'] }}}">
						                                </div>
						                                <div class="form-group">
						                                    <label>
						                                        Choose page:
						                                    </label>
						                                    <select name="page" id="page" class="form-control">
						                                        <option value="0" {{{ ($video['page'] == 0) ? 'selected' : '' }}}>Test Yourself</option>
						                                        <option value="1" {{{ ($video['page'] == 1) ? 'selected' : '' }}}>What you do want done?</option>
						                                    </select>
						                                </div>
						                                <div class="form-group">
						                                    <label>
						                                        Active:
						                                    </label>
						                                    <input type="checkbox" name="activevideo" id="activevideo" {{{ ($video['active'] == 1) ? 'checked': '' }}}>
						                                </div>
						                                <div class="form-group left">
						                                	<input type="hidden" name="id_video" value="{{{ $video['id'] }}}">
						                                	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						                                    <input class="btn btn-sm btn-primary" type="submit" name="editvideo" value="Edit Video" for="#form_{{{ $video['id'] }}}">
						                                </div>
						                            </div>
									            </form>
									        </div>
									            <div class="modal-footer">
									            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									        </div>
									    </div>
								    </div>
								</div>
					            @endforeach
					        @endif
							</tbody>
						</table>
                    </div>
                </div>
            </div>
        </div>
	</div>
	<div class="row">
		<div class="panel panel-default">
            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> List flash</div>
            <div class="panel-body">
                <div class="margin10">
                	<div class="form-group col-sm-6 center">
			        	@if(isset($result2))
		        		   @if($result2)
                            <div class="alert alert-success">
                                {{{ $message2 }}}
                            </div>
                            @else
                            <div class="alert alert-danger">
                                {{{ $message2 }}}
                            </div>
                            @endif
			            @endif
	        		</div>
	        		<br/>
                    <div class="padding5 clearfix"></div>
                    <div class="col-sm-12">
                    	<table id="list_flash" class="table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
					                <th>STT</th>
					                <th>Link</th>
					                <th>Active</th>
					                <th>Type</th>
					                <th>Page</th>
					                <th>Author</th>
					                <th>Update date</th>
					                <th>Action</th>
					            </tr>
							</thead>
							<tbody>
							@if(isset($flashs))
								@foreach ($flashs as $keyflash => $flash)
								<tr id="{{{ $keyflash }}}">
					                <td>{{{ ++$i }}}</td>
					                <td><img src="{{{ url($flash['link']) }}}" alt="" width="150px" ></td>
					                @if($flash['active'] == 1)
					                	<td><span class="label label-success">Active</span></td>
					                @else
					                	<td><span class="label label-danger">No Active</span></td>
					                @endif
					                <td>{{{ ($flash['type'] == 0) ? 'Video' : 'Flash' }}}</td>
					                <td>{{{ ($flash['page'] == 0) ? 'Test Yourself' : 'You Want Done' }}}</td>
					                <td>{{{ $flash['nameuser'] }}}</td>
					                <td>{{{ $flash['updated_at'] }}}</td>
					                <td>
										<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit_flash_{{ $flash['id'] }}">Edit</button>
										<a href="javascript:void(0);" data-target="#confirm-delete_flash_{{{ $flash['id'] }}}" data-toggle="modal" data-href="{{{ url('admin/delete-category/') }}}/{{{ $flash['id'] }}}">
                                            <button stt="{{{ $flash['id'] }}}" type="button" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                                        </a>
                                        <div id="confirm-delete_flash_{{{ $flash['id'] }}}" class="modal fade" role="dialog">
										    <div class="modal-dialog">
											    <!-- Modal content-->
											    <div class="modal-content">
											        <div class="modal-header">
											            <button type="button" class="close" data-dismiss="modal">&times;</button>
											            <h4 class="modal-title">Are you sure to delete this flash ?</h4>
											        </div>
											        <div class="modal-body">
											        	<form action="" method="post" accept-charset="utf-8" id="form_delete_flash_{{{ $flash['id'] }}}">
												            <div class="form-group center">
												            	<input type="hidden" name="id_delete_flash" value="{{{ $flash['id'] }}}">
												        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
												        		<input name="btn_delete_flash" for="#form_delete_flash_{{{ $flash['id'] }}}" type="submit" class="btn btn-danger" value="OK">
												            	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
												            </div>
											        	</form>
											        </div>
											    </div>
										    </div>
										</div>		               	
					                </td>
					            </tr>
					            <div id="edit_flash_{{{ $flash['id'] }}}" class="modal fade" role="dialog">
								    <div class="modal-dialog">
								        <div class="modal-content">
								            <div class="modal-header">
									            <button type="button" class="close" data-dismiss="modal">&times;</button>
									            <h4 class="modal-title">Edit </h4>
									        </div>
									        <div class="modal-body">
									            <form action="" role="form" enctype="multipart/form-data" method="post" accept-charset="utf-8" id="flash_{{{ $flash['id'] }}}">
									            	<div class="form-group">
					                                    <label>
					                                        Link:
					                                    </label>
					                                    <input type="text" class="form-control" name="readonlyflash_{{ $flash['id'] }}" value="{{{ $flash['link'] }}}" placeholder="" readonly="">
					                                </div>
					                                <div class="form-group">
					                                    <label>
					                                        Upload New Flash:
					                                    </label>
					                                    <input type="file" name="inp_flash_{{ $flash['id'] }}" data-show-upload="false">
					                                </div>
					                                <div class="form-group">
					                                    <label>
					                                        Choose page:
					                                    </label>
					                                    <select name="page" class="form-control">
					                                        <option value="0" {{{ ($flash['page'] == 0) ? 'selected' : '' }}}>Test Yourself</option>
					                                        <option value="1" {{{ ($flash['page'] == 1) ? 'selected' : '' }}}>What you do want done?</option>
					                                    </select>
					                                </div>
					                                <div class="form-group">
					                                    <label>
					                                        Active:
					                                    </label>
					                                    <input type="checkbox" name="activeflash" id="activeflash" {{{ ($flash['active'] == 1) ? 'checked' : '' }}}>
					                                </div>
					                                <div class="form-group left">
					                                		<input type="hidden" name="_token" value="{{ csrf_token() }}">
					                                		<input type="hidden" name="id_flash" value="{{{ $flash['id'] }}}">
					                                        <input class="btn btn-sm btn-primary" type="submit" name="editflash" value="Edit Flash" form="flash_{{{ $flash['id'] }}}">
					                                </div>
									            </form>
									        </div>
									        <div class="modal-footer">
									            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									        </div>
									    </div>
								    </div>
								</div>
					            @endforeach
					        @endif
							</tbody>
						</table>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
@stop
