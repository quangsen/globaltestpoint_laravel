@extends ('admin') @section('title') Globaltestpoint - Maintance Mode @stop @section ('content')
<div class="container config_site">
    <form action="" method="post">
    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
    	<input type="hidden" name="_method" value="POST">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> MAINTAINCE FOR WEBSITE</div>
                <div class="panel-body">
                    <div class="margin10">
                        <div class="main_option">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="maintenance" value="1" {{ isset($config) && $config['maintenance'] == 1 ? 'checked' : '' }}> Maintaince
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary">Save change</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@stop
