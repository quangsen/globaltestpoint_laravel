@extends('admin')
@section('title') Globaltestpoint - Partners
@stop
@include('_include_angular')
@section('content')
<div id="partner" ng-app="app" ng-controller="AdminPartnerCtrl as vm">
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2>Partners</h2>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Business Name</th>
                                <th>Email Address</th>
                                <th>Location</th>
                                <th>Area of interest</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>Website</th>
                                <th>Review Link(s)</th>
                                <th>Status</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="partner in vm.partners">
                                <td ng-bind="$index + 1"></td>
                                <td ng-bind="partner.first_name"></td>
                                <td ng-bind="partner.business_name"></td>
                                <td ng-bind="partner.email"></td>
                                <td ng-bind="partner.country.data.name"></td>
                                <td>@{{partner.city}}, @{{partner.state.data.name}}</td>
                                <td ng-bind="partner.address"></td>
                                <td ng-bind="partner.phone"></td>
                                <td ng-bind="partner.website"></td>
                                <td ng-bind="partner.review_links"></td>
                                <td class="text-center">
                                    <span ng-if="partner.accept" class="status_activated">Activated</span>
                                    <span ng-if="!partner.accept" class="status_waiting">Waiting</span>
                                </td>
                                <td>
                                    <button title="Cancel partner" ng-if="partner.accept" class="btn btn-default action" ng-click="vm.active(partner, {accept: 0})"><span class="glyphicon glyphicon-minus"></span></button>
                                    <button title="Accept partner" ng-if="!partner.accept" class="btn btn-success action" ng-click="vm.active(partner, {accept: 1})"><span class="glyphicon glyphicon-ok"></span></button>
                                    <button title="Delete partner" class="btn btn-danger action" ng-click="vm.delete(partner)"><span class="glyphicon glyphicon-remove"></span></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
