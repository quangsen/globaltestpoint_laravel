<div class="form-group">
    <label for="cate_id"><span class="required">*</span> Category</label>
    <select name="cate_id" id="cate_id" class="form-control">
        <option value="">Select a category</option>
        @if (!empty($categories))
        @foreach($categories as $category)
        <option value="{{$category['id']}}" {{ isset($document['cate_id']) && $document['cate_id'] == $category['id'] ? 'selected' : '' }} >{{$category['name']}}</option>
        @endforeach
        @endif
    </select>
</div>
<div class="form-group">
    <label for="image"><span class="required">*</span> Image</label>
    <input type="file" class="form-control document-image" id="image" name="image">
</div>
<div class="form-group">
    <label for="document"><span class="required">*</span> Document</label>
    <input type="file" class="form-control file-document" id="document" name="document">
</div>
<div class="form-group">
    <label for="name"><span class="required">*</span> Name</label>
    <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{ isset($document['name']) && $document['name'] != null ? $document['name'] : old('name') }}"> </div>
<div class="form-group">
    <label for="price"><span class="required">*</span> Price (USD)</label>
    <input type="text" name="price" class="form-control" id="price" placeholder="Price" value="{{ isset($document['price']) && $document['price'] != null ? $document['price'] : old('price') }}"> </div>
<div class="form-group">
    <label for="order">Order</label>
    <input type="text" name="order" class="form-control" id="order" placeholder="Order" value="{{ isset($document['order']) && $document['order'] != null ? $document['order'] : 0 }}"> </div>
<div class="form-group">
    <label for="active">Active</label>
    <input name="active" id="active" type="checkbox" value="1" {{ isset($document[ 'active']) && $document[ 'active'] == 1 ? 'checked': 'checked' }}></input>
</div>
<hr>
<div class="form-group">
    <span class="glyphicon glyphicon-cog"></span> Introduction and instruction on the books</div>
        <textarea class="rich_editor" name="introduction_book" id="introduction_book">{!! (isset($document['introduction_book']) && !empty($document['introduction_book']) ? $document['introduction_book'] : '') !!}</textarea>
        <script>
            ckeditor('introduction_book');
        </script>
    </div>
</div>
<button type="submit" class="btn btn-primary">{{ isset($buttonTitle) && $buttonTitle != null ? $buttonTitle : 'Submit' }}</button>
<br><br>
<script type="text/javascript">
    $(document).ready(function() {
        @if (isset($document))
        $(".document-image").fileinput({
            'showUpload': false,
            'showRemove': false,
            initialPreview: [
                "<img src='{{ asset($document['image']) }}' class='file-preview-image img-responsive img-thumbnail' alt='{{ $document['name'] }}' title='{{ $document['name'] }}'>",
            ],
        });

        $(".file-document").fileinput({
            'showUpload': false,
            'showRemove': false,
            previewFileIcon: '<i class="fa fa-file"></i>',
            initialPreview: [
                "<object src='{{ asset($document['document']) }}' class='file-preview-image img-responsive img-thumbnail' alt='{{ $document['name'] }}' title='{{ $document['name'] }}'>",
            ],
            allowedPreviewTypes: ['image', 'html', 'text', 'video', 'audio', 'flash', 'object']
        });
        @else
        $(".document-image").fileinput({
            'showUpload': false,
            'showRemove': false
        });

        $(".file-document").fileinput({
            'showUpload': false,
            'showRemove': false,
            allowedPreviewTypes: ['image', 'html', 'text', 'video', 'audio', 'flash', 'object']
        });
        @endif
    });
</script>