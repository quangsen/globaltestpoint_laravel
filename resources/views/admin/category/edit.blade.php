@extends ('admin') @section('title') Add Category @stop @section ('content')
<div class="container index_category">
    @include('blocks/error')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span>Add New Category</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <a href="{{{ url('/admin/category') }}}">
                            <button type="button" class="btn btn-primary">
                                <i class="fa fa-backward" aria-hidden="true"></i> Back
                            </button>
                        </a>
                    </div>
                </div>
                <br /> @if(isset($result))
                <div class="form-group col-sm-12 col-md-12 center">
                    @if($result)
                    <div class="alert alert-success">
                        <strong>Successful !!</strong> Added a new Category.
                    </div>
                    @else
                    <div class="alert alert-danger">
                        <strong>Error !!</strong> Not add a Category.
                    </div>
                    @endif @if (count($errors) > 0)
                    <div class="alert alert-danger" style="text-align: left">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
                @endif
                <form action="" method="post" role="form" class="form-horizontal" accept-charset="utf-8">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="PUT">
                    <fieldset>
                        <div class="form-group">
                            <label for="parent" class="col-sm-12 col-md-3 control-label">Category Parent: </label>
                            <div class="col-sm-12 col-md-6">
                                <select id="parent" name="parent_id" class="form-control">
                                    <option value="">Please choose Category</option>
                                    <?php category_parent($categories, 0, '--', $category['parent_id']); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-12 col-md-3 control-label"><span class="required">*</span> Category Name: </label>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" value="{!! isset($category['name']) && $category['name'] != null ? $category['name'] : old('name') !!}" class="form-control" name="name" autocomplete="off" id="name" placeholder="Category Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="type" class="col-sm-12 col-md-3 control-label"><span class="required">*</span> Type: </label>
                            <div class="col-sm-12 col-md-6">
                                <select id="type" name="type" class="form-control">
                                    <option value="">Please select a type</option>
                                    @foreach($types as $type)
                                    <option value="{!! $type->name !!}" {{ $category[ 'type']== $type->name ? 'selected' : '' }} >{!! $type->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="page" class="col-sm-12 col-md-3 control-label"><span class="required">*</span> Belong to Page: </label>
                            <div class="col-sm-12 col-md-6">
                                <select id="page" name="page_id" class="form-control">
                                    <option value="2" {{ $category[ 'page_id']==2 ? 'selected' : '' }}>Test Your Self</option>
                                    <option value="3" {{ $category[ 'page_id']==3 ? 'selected' : '' }}>What Do You Want done</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="order" class="col-sm-12 col-md-3 control-label"> Order: </label>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" value="{!! isset($category['order']) && $category['order'] != null ? $category['order'] : old('order') !!}" class="form-control" name="order" id="order">
                            </div>
                        </div>
                        <div class="form-group intro_exams_container">
                            <label for="order" class="col-sm-12 col-md-3 control-label"> Introduction and instruction on the exams: </label>
                            <div class="col-sm-12 col-md-6">
                                <textarea class="rich_editor" name="introduction_exam" id="introduction_exam">{!! (isset($category['introduction_exam']) && !empty($category['introduction_exam']) ? $category['introduction_exam'] : '') !!}</textarea>
                                <script>
                                    ckeditor('introduction_exam');
                                </script>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="is_active" class="col-sm-12 col-md-3 control-label">Status: </label>
                            <div class="col-sm-12 col-md-9">
                                <label style="margin-top: 5px;">
                                    <input type="checkbox" value="1" {{ $category[ 'active']==1 ? 'checked' : '' }} name="active" id="active"> Active
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="col-sm-4 col-md-4 col-sm-offset-3 col-md-offset-3">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var val_name_first = $('#type option:selected').val();
        console.log(val_name_first);
        if(val_name_first == 'Exam') {
            $('.intro_exams_container').removeClass('hide');
        } else {
            $('.intro_exams_container').addClass('hide'); 
        }
        $('#type').change(function(){
            var val_name = $('#type option:selected').val();
            console.log(val_name);
            if(val_name == 'Exam') {
                $('.intro_exams_container').removeClass('hide');
            } else {
                $('.intro_exams_container').addClass('hide');
            }
        });
    });
</script>
@stop
