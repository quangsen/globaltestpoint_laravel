@extends ('admin')

@section('title')
    Globaltestpoint - Dashboard Category
@stop

@section ('content')
<div class="container">
	<div class="row">
		<div class="panel panel-default">
            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> List slider</div>
            <div class="panel-body">
                <div class="margin10">
                	<div class="col-sm-12">
                    	<table id="list_slider" class="table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
					                <th>STT</th>
					                <th>Image</th>
					                <th>Active</th>
					                <th>Sort</th>
					                <th>Page</th>
					                <th>Action</th>
					            </tr>
							</thead>
							<tbody>
							<?php $i = 0; ?>
							@if(isset($sliders))
								@foreach ($sliders as $key => $slider)
								<tr id="{{{ $slider['id'] }}}">
					                <td>{{{ ++$i }}}</td>
					                <td><img src="{{ asset($slider['image'])}}" width="100px" height="100px" /></td>
					                @if($slider['active'] == 1)
					                <td><span class="label label-success">Active</span></td>
					                @else
					                <td><span class="label label-danger">No Active</span></td>
					                @endif
					                <td>{{ $slider['sort'] }}</td>
					                @if($slider['page'] == 1)
					                <td><span class="label label-success">Home Page</span></td>
					                @elseif($slider['page'] == 2)
					                <td><span class="label label-info">Test Yourself</span></td>
					                @else
					                <td><span class="label label-danger">You Want Done</span></td>
					                @endif
					                <td>
					                	<a href="{{{ url('admin/slider/edit') }}}/{{{ $slider['id'] }}}">
                                            <button type="button" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                                        </a>
                                        <a href="javascript:void(0);" data-target="#confirm-delete2_{{{ $slider['id'] }}}" data-toggle="modal" >
                                            <button stt="{{{ $slider['id'] }}}" type="button" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                                        </a>
                                        <div id="confirm-delete2_{{{ $slider['id'] }}}" class="modal fade" role="dialog">
										    <div class="modal-dialog">
											    <!-- Modal content-->
											    <div class="modal-content">
											        <div class="modal-header">
											            <button type="button" class="close" data-dismiss="modal">&times;</button>
											            <h4 class="modal-title">Are you sure to delete this slider ?</h4>
											        </div>
											        <div class="modal-body">
											        <form action="" method="post" accept-charset="utf-8">
											        	<div class="form-group center">
											        		<input type="hidden" name="id_delete_slider-2" value="{{{ $slider['id'] }}}">
											        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
											        		<input type="submit" class="btn btn-danger" value="OK" id="btn-delete-slider-2">
											            	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
											            </div>
											        </form>
											        </div>
											    </div>
										    </div>
										</div>
					                </td>
					            </tr>
					            @endforeach
					        @endif
							</tbody>
						</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
