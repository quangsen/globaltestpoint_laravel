@extends ('admin')

@section('title')
    Update Slider - Globaltestpoint
@stop

@section ('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-cog">
                </span>
                ADD SLIDER
            </div>
            <div class="panel-body">
                <div class="margin10">
                	<a href="{{ url('admin/slider') }}" title="">
                        <button class="btn btn-primary" type="button">
                            List Slider
                        </button>
                    </a>
                    @include('blocks.error')
                    <div class="padding5 clearfix">
                    </div>
                    <br>
                    <div class="col-sm-12 col-md-12 center">
                        <form accept-charset="utf-8" action="" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-sm-12 col-md-6 center">
                                    <div class="form-group">
                                    	<div class="col-sm-12 col-md-12 mgr-bottom15 div-image">
                                            @if(isset($slider['image']))
                                            <label>
                                                Image slide:
                                            </label>
                                            <img src="{{ url($slider['image']) }}" alt="slider" id="img_slider" width="200px" height="200px">
                                            <input type="hidden" name="old_image" value="{{ url($slider['image']) }}">
                                            <br />
                                            @endif
                                            <label>
                                                Change new slide:
                                            </label>
    	                                    <input id="image_slider" data-show-upload="false" name="image" type="file">
    	                                </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 col-md-12 mgr-bottom15">
                                            <label class="control-label" for="parent">
                                                <span class="required">
                                                    *
                                                </span>
                                                Page:
                                            </label>
                                            <select class="form-control" name="page">
                                                <option {{ $slider['page'] == 1 ? 'selected' : '' }} value="1">
                                                    Home Page
                                                </option>
                                                <option {{ $slider['page'] == 2 ? 'selected' : '' }} value="2">
                                                    Test Yourself
                                                </option>
                                                <option value="3" {{ $slider['page'] == 3 ? 'selected' : '' }}>
                                                    You Want Done
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 col-md-12 mgr-bottom15">
                                            <label for="">
                                                Sort:
                                            </label>
                                            <input class="form-control" id="sort" name="sort" placeholder="" type="number" value="{{ $slider['sort'] }}">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 col-md-12 mgr-bottom15">
                                            <label style="margin-top: 5px;">
                                                <input {{ ($slider['active'] == 1)? 'checked' : '' }} id="active_slider" name="active_slider" type="checkbox">
                                                    Active
                                                </input>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    	<div class="col-sm-12 col-md-6 center">
    	                                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
    	                                        <input class="btn btn-primary" name="btn_option" type="submit" value="Update Slide">
    	                                        </input>
    	                                    </input>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
