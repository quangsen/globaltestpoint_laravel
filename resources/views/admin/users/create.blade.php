@extends('admin') 
@section('title') Globaltestpoint - Partners 
@stop 
@include('_include_angular')
@section('content')
<div class="container" ng-app="app" ng-controller="AdminUserCtrl as vm">
    <div class="col-xs-12 col-sm-8 col-sm-6 col-sm-offset-2 col-sm-offset-3">
        <h3>Create new user</h3>
        <form method="post" name="vm.create_user_form" ng-submit="vm.create_user_form.$valid && vm.create(vm.user)" novalidate>
            <div class="form-group">
                <label>Role</label>
                <select name="role" class="form-control" required ng-model="vm.user.role" ng-options="role as role.name for role in vm.roles"></select>
                <div class="errors">
                    <span ng-show="vm.create_user_form.$submitted && vm.create_user_form.role.$error.required">Required</span>
                </div>
            </div>
            <div class="form-group">
                <label>First Name</label>
                <input type="text" class="form-control" name="first_name" ng-model="vm.user.first_name" required>
                <div class="errors">
                    <span ng-show="vm.create_user_form.$submitted && vm.create_user_form.first_name.$error.required">Required</span>
                </div>
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <input type="text" class="form-control" name="last_name" ng-model="vm.user.last_name" required>
                <div class="errors">
                    <span ng-show="vm.create_user_form.$submitted && vm.create_user_form.last_name.$error.required">Required</span>
                </div>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" name="email" ng-model="vm.user.email" required>
                <div class="errors">
                    <span ng-show="vm.create_user_form.$submitted && vm.create_user_form.email.$error.required">Required</span>
                    <span ng-show="vm.create_user_form.$submitted && vm.create_user_form.email.$error.email">Please enter a valid email address</span>
                </div>
            </div>
            <div class="form-group">
                <label>Mobile</label>
                <input type="text" class="form-control" name="mobile" ng-model="vm.user.mobile">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="text" class="form-control margin-bottom-10" name="password" ng-model="vm.user.password" required>
                <div class="errors">
                    <span ng-show="vm.create_user_form.$submitted && vm.create_user_form.password.$error.required">Required</span>
                </div>
                <a class="btn btn-default" ng-click="vm.generate()">generate</a>
            </div>
            <div class="form-group">
                <button class="btn btn-primary" ng-class="{'disabled': vm.inprocess}" ng-disable="vm.inprocess">Submit</button>
            </div>
        </form>
        <div class="alert alert-success" ng-show="vm.success">
            <strong>Success!</strong> New user was created.
        </div>
    </div>
</div>
@stop
