@extends('admin') @section('title') Globaltestpoint - List User @stop @section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> Add New Category
            </div>
            <div class="panel-body">
                <h2>Users Listing</h2>
                <table class="table table-bordered" id="list_user">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Full Name</th>
                            <th>Location</th>
                            <th>Email</th>
                            <th>Date of birth</th>
                            <th>Phone</th>
                            <th>Date Joined</th>
                            <th class="text-center">Active</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $stt = 0; ?> @foreach ($users as $val)
                        <?php $stt++; ?>
                        <tr>
                            <td>{!! $stt !!}</td>
                            <td>{!! $val['first_name'] !!}</td>
                            <td>{!! $val['country'] !!}</td>
                            <td>{!! $val['email'] !!}</td>
                            <td>{!! $val['birth'] !!}</td>
                            <td>{!! $val['mobile'] !!}</td>
                            <td>
                                <?php
                           $date = date_create($val['created_at']);
                           echo date_format($date, "d/m/Y"); 
                        ?>
                            </td>
                            @if($val['active'] == 1)
                            <td class="text-center"><span class="label label-success">Active</span></td>
                            @else
                            <td class="text-center"><span class="label label-danger">No Active</span></td>
                            @endif
                            <td>
                                <a href="{!! URL::route('admin.users.getEdit', $val['id']) !!}">
                                    <button type="button" class="btn btn-info">Edit</button>
                                </a>
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_user_{{ $val['id'] }}">Delete</button>
                            </td>
                        </tr>
                        <div class="modal fade" id="delete_user_{{ $val['id'] }}" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Delete</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Are you sure you want to delete this user?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="{!! URL::route('admin.users.getDelete', $val['id']) !!}">
                                            <button type="button" class="btn btn-danger">Yes</button>
                                        </a>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop
