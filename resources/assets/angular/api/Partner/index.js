(function(app) {
    app.service('PartnerService', PartnerService);
    PartnerService.$inject = ['$http', '$resource', '$q'];

    function PartnerService($http, $resource, $q) {
        var url = '/api/partners/:id';
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.get = function(params) {
            params = params || {};
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.create = function(partner) {
            var deferred = $q.defer();
            partner = partner || {};
            resource.create({}, partner, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.delete = function(partner_id) {
            var deferred = $q.defer();
            var url = '/api/partners/' + partner_id;
            $http.delete(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.active = function(partner_id, params) {
            params = params || {};
            var deferred = $q.defer();
            var url = '/api/partners/' + partner_id + '/active';
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

    }
})(angular.module('app.api.partner', []));
