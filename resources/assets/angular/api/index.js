require('./User/');
require('./Partner/');
require('./Role/');
(function(app) {
    app.factory('API', theFactory);

    theFactory.$inject = [
        'UserService',
        'PartnerService',
        'RoleService'
    ];

    function theFactory(
        UserService,
        PartnerService,
        RoleService
    ) {
        return {
            user: UserService,
            partner: PartnerService,
            role: RoleService
        };
    }
})(angular.module('app.api', [
    'app.api.partner',
    'app.api.user',
    'app.api.role'
]));
