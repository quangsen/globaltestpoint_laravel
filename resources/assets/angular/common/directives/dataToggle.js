(function(app) {

    app.directive('myToggle', function() {
        return {
            restrict: 'AE',
            link: function(scope, element, attrs) {
                $(element).click(function() {
                	var toggleElement = $(element).attr('toggle-element');
                    $(toggleElement).slideToggle();
                });
            }
        };
    });

})(angular.module('app.common.directives.dataToggle', []));
