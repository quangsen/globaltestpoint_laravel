var Dropzone = require('dropzone');
require('ng-file-upload');

(function(app) {
    app.directive('dropzone', ['$rootScope', function($rootScope) {
        return {
            strict: 'AE',
            scope: {
                onSuccess: '=',
                onError: '=',
                modelName: '=',
                previewImage: '=',
                imgSrc: '=',
                folderUpload: '@'
            },
            link: function(scope, element, attrs) {
                scope.$watch('modelName', function(modelName) {
                    console.log(modelName);
                });

                var config = {
                    url: '/file/upload',
                    paramName: "file",
                };

                var eventHandlers = {

                    'sending': function(file, xhr, formData) {
                        if (scope.folderUpload !== undefined) {
                            formData.append('folderUpload', scope.folderUpload);
                        }
                    },

                    'success': function(file, response) {
                        if (scope.modelName !== undefined) {
                            scope.modelName = response.data;
                        }
                        scope.imgSrc = $rootScope.asset(response.data);
                        scope.$apply();
                    },

                    'queuecomplete': function() {
                        this.removeAllFiles();
                    },

                    'error': function(file, err) {
                        if (scope.previewImage !== undefined) {
                            scope.previewImage = false;
                        }
                        scope.onError(err, file);
                        this.removeAllFiles();
                    }

                };

                dropzone = new Dropzone(element[0], config);

                angular.forEach(eventHandlers, function(handler, event) {
                    dropzone.on(event, handler);
                });
            },
        };
    }]);
})(angular.module('app.common.directives.dropzone', ['ngFileUpload']));
