(function(app) {

    app.directive('deleteCkeditor', function() {
        return {
            restrict: 'AE',
            scope: {
                deleteCkeditor: '@',
                autoDelete: '@'
            },
            link: function(scope, element, attrs) {
                scope.$watch('autoDelete', function(autoDelete) {
                    if (scope.deleteCkeditor !== undefined) {
                        if (autoDelete === 'true') {
                            var hEd = CKEDITOR.instances[scope.deleteCkeditor];
                            if (hEd) {
                                hEd.destroy();
                            }
                        }
                    }
                });

                scope.$watch('deleteCkeditor', function(deleteCkeditor) {
                    if (deleteCkeditor !== undefined) {
                        $(element).click(function() {
                            var hEd = CKEDITOR.instances[scope.deleteCkeditor];
                            if (hEd) {
                                hEd.destroy();
                            }
                        });
                    }
                });
            }
        };
    });

})(angular.module('app.common.directives.deleteCkeditor', []));
