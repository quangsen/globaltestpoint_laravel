(function(app) {
    app.directive('select2', ['$rootScope', function($rootScope) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                $(element).select2();
                $(element).on('change', function(event) {
                    $rootScope.$broadcast('select2selected', {
                        name: attrs.name,
                        val: $(element).val()
                    });
                });
            }
        };
    }]);
})(angular.module('app.common.directives.select2', []));
