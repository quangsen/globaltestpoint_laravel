require('./admin/');
require('./home/');
require('./password/forgot');
require('./partner/');
(function(app) {

})(angular.module('app.components', [
    'app.components.admin',
    'app.components.home',
    'app.components.password.forgot',
    'app.components.partner',
]));
