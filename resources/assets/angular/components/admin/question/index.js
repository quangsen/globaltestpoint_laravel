var Answer = function(options) {
    options = options || {};
    this.answer = options.answer || '';
    this.is_correct = options.is_correct || false;
    this.order = options.order || '';
    this.description = options.description || '';
};
var Question = function(options) {
    options = options || {};
    this.question = options.question || '';
    this.image = options.image || '';
    this.description = options.description || '';
    this.answers = options.answers || [];

    // 1 is text, 2 is image
    this.type = options.type || 1;
};
(function(app) {

    app.controller('UpdateQuestionCtrl', UpdateQuestionCtrl);
    UpdateQuestionCtrl.$inject = ['$rootScope', '$scope', '$http', '$window', '$cookieStore'];

    function UpdateQuestionCtrl($rootScope, $scope, $http, $window, $cookieStore) {
        var vm = this;

        var flashClass = $cookieStore.get('flashClass');
        var flashMessage = $cookieStore.get('flashMessage');
        if (flashClass !== undefined) {
            vm.flashClass = flashClass;
            $cookieStore.remove('flashClass');
        }
        if (flashMessage !== undefined) {
            vm.flashMessage = flashMessage;
            $cookieStore.remove('flashMessage');
        }

        function getExams() {
            var url = 'api/exams';
            url = $rootScope.asset(url);
            $http.get(url)
                .then(function(response) {
                    vm.exams = response.data.data;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getQuestion(question_id) {
            var url = 'api/question/' + question_id;
            url = $rootScope.asset(url);
            $http.get(url)
                .then(function(response) {
                    vm.question = response.data.data;
                    vm.question.type = parseInt(vm.question.type);
                    if (vm.question.type === 2) {
                        vm.question.image = angular.copy(vm.question.question);
                        vm.question.question = '';
                    }
                    vm.dataQuestion = angular.copy(vm.question);
                    $scope.$watch('vm.exams', function(exams) {
                        if (exams !== undefined) {
                            vm.question.exam = _.find(exams, { 'id': vm.question.exam.id });
                        }
                    });
                }, function(error) {
                    throw error;
                });
        }

        getExams();
        $scope.$watch('question_id', function(question_id) {
            if (question_id !== undefined) {
                getQuestion(question_id);
            }
        });

        vm.removeAnswer = function(answer) {
            if (vm.question.answers.data.length < 2) {
                alert("At least 1 answer need to add");
                return false;
            }
            if ($window.confirm('Are you sure to delete this answer ?')) {
                if (answer.id !== undefined) {
                    var url = 'admin/answer/delete/' + answer.id;
                    url = $rootScope.asset(url);
                    $http.get(url)
                        .then(function(result) {
                            vm.question.answers.data = _.pull(vm.question.answers.data, answer);
                        })
                        .catch(function(error) {
                            throw error;
                        });
                } else {
                    vm.question.answers.data = _.pull(vm.question.answers.data, answer);
                }
            }
        };

        vm.addMoreAnswer = function(question) {
            vm.question.answers.data.push(new Answer());
        };

        vm.updateQuestion = function(question) {
            vm.inProgress = true;
            vm.dataQuestion.type = question.type;
            vm.dataQuestion.question = question.question;
            vm.dataQuestion.image = question.image;
            vm.dataQuestion.answers = question.answers;
            vm.dataQuestion.solution = question.solution;

            if (vm.dataQuestion.type === 2 && vm.dataQuestion.image === '') {
                $window.alert('Please select an image');
                return false;
            }
            if (vm.dataQuestion.type === 2 && vm.dataQuestion.image !== '') {
                vm.dataQuestion.question = vm.dataQuestion.image;
            }
            $http.post($window.location.href, vm.dataQuestion)
                .then(function(result) {
                    vm.inProgress = false;
                    vm.updateQuestionForm.$setPristine();
                    $cookieStore.put('flashClass', 'alert-success');
                    $cookieStore.put('flashMessage', 'Question/options updated successfully!!!');
                    $window.location.reload();
                })
                .catch(function(error) {
                    vm.inProgress = false;
                    throw error;
                });
        };
    }

})(angular.module('app.components.admin.question', []));
