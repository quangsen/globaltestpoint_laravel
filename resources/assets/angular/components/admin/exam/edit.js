var Grade = function(options) {
    options = options || {};
    this.title = options.title || '';
    this.min = options.min || 0;
    this.max = options.max || 0;
};

(function(app) {

    app.controller('EditExamCtrl', EditExamCtrl);
    EditExamCtrl.$inject = ['$rootScope', '$scope', '$window'];

    function EditExamCtrl($rootScope, $scope, $window) {
        var vm = this;

        $scope.$watch('vm.exam', function(exam) {
            if (exam !== undefined) {
                vm.grades = JSON.parse(exam.grade);
            }
        });

        vm.addGrade = function() {
            if (vm.grades === undefined) {
                vm.grades = [];
            }
            vm.grades.push(new Grade());
            console.log(vm.grades);
        };

        vm.removeGrade = function(grade) {
            if (vm.grades.length < 2) {
                alert("At least 1 grade need to add");
                return false;
            }
            if ($window.confirm('Are you sure to delete this grade ?')) {
                vm.grades = _.pull(vm.grades, grade);
            }
        };
    }

})(angular.module('app.components.admin.exam.edit', []));
