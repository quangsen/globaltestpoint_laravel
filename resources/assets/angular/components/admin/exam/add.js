var Grade = function(options) {
    options = options || {};
    this.title = options.title || '';
    this.min = options.min || 0;
    this.max = options.max || 0;
};

(function(app) {

    app.controller('AddExamCtrl', AddExamCtrl);
    AddExamCtrl.$inject = ['$rootScope', '$scope', '$window'];

    function AddExamCtrl($rootScope, $scope, $window) {
        var vm = this;
        vm.grades = [];
        vm.grades.push(new Grade());

        vm.addGrade = function() {
            if (vm.grades === undefined) {
                vm.grades = [];
            }
            vm.grades.push(new Grade());
        };

        vm.removeGrade = function(grade) {
            if (vm.grades.length < 2) {
                alert("At least 1 grade need to add");
                return false;
            }
            if ($window.confirm('Are you sure to delete this grade ?')) {
                vm.grades = _.pull(vm.grades, grade);
            }
        };
    }

})(angular.module('app.components.admin.exam.add', []));
