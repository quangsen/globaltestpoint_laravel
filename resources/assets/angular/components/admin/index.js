require('./exam/');
require('./exam/add');
require('./exam/edit');
require('./question/');
require('./partner/');
require('./user/');
(function(app) {

})(angular.module('app.components.admin', [
    'app.components.admin.exam',
    'app.components.admin.exam.add',
    'app.components.admin.exam.edit',
    'app.components.admin.question',
    'app.components.admin.partner',
    'app.components.admin.user',
]));
