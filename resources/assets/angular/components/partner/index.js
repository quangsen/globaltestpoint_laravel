(function(app) {
    app.controller('PartnerCtrl', PartnerCtrl);

    PartnerCtrl.$inject = ['$rootScope', '$scope', 'API'];

    function PartnerCtrl($rootScope, $scope, API) {
        var vm = this;
        vm.countries = countries;

        $rootScope.$on('userLoaded', function(event, user) {
            vm.partner = user;
            vm.selectedCountry = countries[0];
            vm.partner.country_id = vm.selectedCountry.id;
            vm.partner.state_id = vm.selectedCountry.states[0].id;
            vm.inprocess = false;

            vm.partner.number_of_users = '10-100';
            vm.partner.interest = 'Academic Test';

            vm.parter_preferences = ['Basic', 'Silver', 'Golden', 'Premium'];

            vm.partner.perference = 'Basic';
        });

        $rootScope.$on('select2selected', function(event, data) {
            if (data.name === 'country_id') {
                vm.selectedCountry = _.find(vm.countries, function(item) {
                    return parseInt(item.id) === parseInt(data.val);
                });
                vm.partner.country_id = data.val;
                vm.partner.state = vm.selectedCountry.states[0].id;
            }
            if (data.name === 'state_id') {
                vm.partner.state_id = data.val;
            }
            $scope.$apply();
        });

        vm.addPartner = function(partner) {
            vm.inprocess = true;
            console.log(partner);
            API.partner.create(partner)
                .then(function(partner) {
                    console.log(partner);
                    vm.partner = {};
                    vm.success = true;
                    vm.inprocess = false;
                })
                .catch(function(error) {
                    alert(error.message);
                    vm.inprocess = false;
                });
        };

        function transform(partner) {
            partner.country_id = partner.country.id;
            partner.state_id = partner.state.id;
            return partner;
        }
    }
})(angular.module('app.components.partner', []));
