<?php

use App\Entities\User;

class UserTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->user = User::create([
            'first_name'     => 'first',
            'last_name'      => 'last',
            'user_type'      => 'student',
            'email'          => 'unit_test' . mt_rand(0, 1000) . '@gmail.com',
            'role'           => 'userRole',
            'active'         => 1,
            'email_verified' => 1,
        ]);
    }
    /**
     * Test User Instance
     *
     * @return void
     */
    public function testCreateUser()
    {
        $this->assertInstanceOf(User::class, $this->user);
    }
    public function testGetUser()
    {
        $this->assertInstanceOf(User::class, User::find($this->user->id));
    }
    public function testDelUser()
    {
        $this->assertTrue($this->user->delete());
    }
}
