<?php

use App\Entities\Exam;

class ExamTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $grade = [
            [
                'title' => 'A',
                'min'   => 70,
                'max'   => 100,
            ],
            [
                'title' => 'B',
                'min'   => 60,
                'max'   => 69,
            ],
            [
                'title' => 'C',
                'min'   => 50,
                'max'   => 59,
            ],
            [
                'title' => 'E',
                'min'   => 40,
                'max'   => 49,
            ],
            [
                'title' => 'F',
                'min'   => 0,
                'max'   => 39,
            ],
        ];
        $grade                    = json_encode($grade);
        $title                    = "Exam " . mt_rand(0, 100);
        $this->exam               = new Exam;
        $this->exam->name         = $title;
        $this->exam->slug         = str_slug($title);
        $this->exam->duration     = mt_rand(10, 60);
        $this->exam->price        = mt_rand(0, 3);
        $this->exam->pass_percent = mt_rand(60, 80);
        $this->exam->grade        = $grade;
        $this->exam->active       = 'yes';
        $this->exam->category_id  = 4;
        $this->exam->save();
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateExam()
    {
        $this->assertInstanceOf(Exam::class, $this->exam);
    }
    public function testDelExam()
    {
        $this->assertTrue($this->exam->delete());
    }
}
