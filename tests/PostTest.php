<?php

use App\Entities\Post;

class PostTest extends TestCase
{
    /**
     * Post Test
     *
     * @return void
     */
    public function testCreatePost()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new Faker\Provider\DateTime($faker));
        $faker->addProvider(new Faker\Provider\Internet($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));
        $post              = new Post;
        $post->title       = $faker->sentence();
        $post->content     = $faker->text();
        $post->slug        = str_slug($post->title);
        $post->active      = mt_rand(0, 1);
        $post->image       = '/uploads/default_post.png';
        $post->image_cover = '/uploads/default_cover_post.jpg';
        $post->author      = 1;
        $post->type        = 'post';
        $post->save();
        $post->categories()->attach(mt_rand(2, 3));
        $this->assertInstanceOf(Post::class, $post);
        $post->delete();
    }
    public function testDelPost()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new Faker\Provider\DateTime($faker));
        $faker->addProvider(new Faker\Provider\Internet($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));
        $post              = new Post;
        $post->title       = $faker->sentence();
        $post->content     = $faker->text();
        $post->slug        = str_slug($post->title);
        $post->active      = mt_rand(0, 1);
        $post->image       = '/uploads/default_post.png';
        $post->image_cover = '/uploads/default_cover_post.jpg';
        $post->author      = 1;
        $post->type        = 'post';
        $post->save();
        $post->categories()->attach(mt_rand(2, 3));
        $this->assertTrue($post->delete());
    }
}
