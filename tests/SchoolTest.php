<?php

use App\Entities\School;

class SchoolTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
    }
    /**
     * School Test
     *
     * @return void
     */
    public function testHasSchoolInDatabase()
    {
        $this->assertNotEmpty(School::get());
    }
    public function testCreate()
    {
        $school = School::create([
            'name'             => 'Test School',
            'image'            => '/uploads/schools/top_uk_private_school_stowe_exterior-01-302.jpg',
            'state_id'         => mt_rand(1, 20),
            'school_type_id'   => mt_rand(1, 4),
            'school_degree_id' => mt_rand(1, 2),
        ]);
        $this->assertInstanceOf(School::class, $school);
    }
    public function testHasTesterRole()
    {
        $school = School::create([
            'name'             => 'Test School',
            'image'            => '/uploads/schools/top_uk_private_school_stowe_exterior-01-302.jpg',
            'state_id'         => mt_rand(1, 20),
            'school_type_id'   => mt_rand(1, 4),
            'school_degree_id' => mt_rand(1, 2),
        ]);
        $this->assertTrue($school->delete());
    }
}
