<?php

namespace App\Http\Middleware;

use App\Entities\Config;
use App\Entities\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CheckForMaintenanceMode {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 *
	 * @throws \Symfony\Component\HttpKernel\Exception\HttpException
	 */
	public function handle($request, Closure $next, $guard = null) {
		$user = Auth::user();
		if (!$user) {
			$config = Config::first();
			if ($config && $config['maintenance'] == 1) {
				throw new HttpException(503);
			}
		}
		return $next($request);
	}
}
