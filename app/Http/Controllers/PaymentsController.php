<?php

namespace App\Http\Controllers;

use App\Entities\Document;
use App\Entities\Exam;
use App\Entities\Paid;
use App\Entities\Subscription;
use App\Entities\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class PaymentsController extends Controller
{
    public function __construct()
    {
        \Stripe\Stripe::setApiKey(getenv('STRIPE_SECRET'));
        // \Stripe\Stripe::setApiKey('sk_test_kgSzgjee6mATIMTwBpJKmmkJ');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type, $id)
    {
        $user = Auth::user();
        if ($type == 'document') {
            $product = Document::find($id);
        } else {
            $product = Exam::find($id);
        }
        if (!empty($user)) {
            $totalprice = $product['price'];
            if (Session::has('tmp_choose')) {
                Session::forget('tmp_choose');
            }
            if (Session::has('backlink')) {
                Session::forget('backlink');
            }
            $backlink = URL::previous();
            if (!empty($user->stripe_id)) {
                $infor = $this->getCustomer($user->stripe_id);
            } else {
                $infor = '';
            }
            if ($user['active'] != 0) {
                return view('testyourself.payment.index', ['customer_info' => $infor, 'totalprice' => $totalprice, 'backlink' => $backlink]);
            } else {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => "You must active your account to execute payment !"]);
            }
        } else {
            return redirect()->route('getLoginUser')->with(['flash_level' => 'danger', 'flash_message' => "You need Login/Register to begin payment !"]);
        }
    }
    /**
     * [payment return status after payment]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function payment($type, $id, Request $request)
    {
        $user = Auth::user();
        if (!empty($user)) {
            $data = [
                'email' => $user->email,
                'description' => 'Billing for ' . $user->last_name . ' from email: ' . $user->email,
            ];
            $token = $request->get('stripeToken');
            //==================================

            if (empty($user->stripe_id)) {

                //=================================================
                $reponseCustomer = $this->createCustomer($data, $token);
                $customer_id = $reponseCustomer->id;
                $saveCustomer = $this->saveCustomerToDatabase($reponseCustomer);
                if (!$saveCustomer) {
                    return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => "Don't Save Information Customer"]);
                }
            } else {
                $customer_id = $user->stripe_id;
            }
            // dd($token);
            //================================================
            //==================================
            if ($type == 'document') {
                $product = Document::find($id);
            } elseif ($type == 'exam') {
                $product = Exam::find($id);
            } else {
                return redirect()->route('/testyourself')->with(['flash_level' => 'danger', 'flash_message' => "URL failed !"]);
            }
            $backlink = $request->get('backlink');
            Session::put('backlink', $backlink);
            if (Session::has('fullUrl')) {
                $redirectDocument = Session::get('fullUrl');
            } else {
                $redirectDocument = $backlink;
            }
            $responseCharge = $this->createCharge(($product['price'] * 100), getenv('STRIPE_CURRENCY'), $customer_id, $data['description']);
            if (isset($responseCharge->status) && $responseCharge->status == "succeeded") {
                $price = ((int) $product['price']);
                $saveCharge = $this->saveTransactionToDatabase($responseCharge);
                if ($saveCharge) {
                    if ($this->savePaidToDatabase($type, $id, $responseCharge->id)) {
                        if ($type == 'document') {
                            $this->sendMailPaymentDocument($user, $price);
                            return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $redirectDocument, 'status' => 1, 'message' => "Payment sucessfully with " . $product['price'] . "$ for '" . $product['name'] . "' document"]);
                        } else {
                            $this->sendMailPaymentExam($user, $product);
                            $doExam = Session::get('doExam');
                            if ($doExam != null) {
                                return view('testyourself.payment.response', ['page' => 'exam', 'backlink' => url('testyourself/exam/' . $product['slug']), 'status' => 1, 'message' => "Payment of " . $product['price'] . "$ for '" . $product['name'] . "' was successful"]);
                                // return redirect('testyourself/exam/' . $product['slug'])->with(['flash_level' => 'success', 'flash_message' => "Successfully ! Payment complete with " . $product['price'] . "$ ! You can continue this exam"]);
                            } else {
                                return view('testyourself.payment.response', ['page' => 'exam', 'backlink' => url('testyourself/exams'), 'status' => 1, 'message' => "Payment of " . $product['price'] . "$ for '" . $product['name'] . "' was successful"]);
                                // return redirect('testyourself/exams')->with(['flash_level' => 'success', 'flash_message' => "Successfully ! Payment complete with " . $product['price'] . "$ ! Now you can test " . $product['name'] . " with full permission."]);
                            }
                        }
                    } else {
                        return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $redirectDocument, 'status' => 0, 'message' => "Don't save information transaction"]);
                    }
                } else {
                    return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $redirectDocument, 'status' => 0, 'message' => "Don't save information transaction"]);
                }
            } else {
                $this->deleteCustomer($customer_id);
                return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $redirectDocument, 'status' => 0, 'message' => "An error occurred while making a payment"]);
            }
        } else {
            return redirect()->route('getLoginUser')->with(['flash_level' => 'danger', 'flash_message' => "You need Login/Register to begin payment !"]);
        }
    }

    /**
     * [multiPayment description]
     * @param  [int] $price       [description]
     * @param  [string] $customer_id [description]
     * @return [boolean]              [description]
     */
    public function multiPayment(Request $request)
    {
        $user = Auth::user();
        if (!empty($user)) {
            $data = [
                'email' => $user->email,
                'description' => 'Billing for ' . $user->last_name . ' from email: ' . $user->email,
            ];
            //==================================
            if (!empty($request->get('stripeToken'))) {
                $token = $request->get('stripeToken');
            } else {
                $token = '';
            }
            //==================================

            if (empty($user->stripe_id)) {

                //=================================================
                $reponseCustomer = $this->createCustomer($data, $token);
                $customer_id = $reponseCustomer->id;
                $saveCustomer = $this->saveCustomerToDatabase($reponseCustomer);
                if (!$saveCustomer) {
                    return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => "Don't Save Information Customer"]);
                }
            } else {
                $customer_id = $user->stripe_id;
            }
            if (Session::has('backlink')) {
                $backlink = Session::get('backlink');
                Session::forget('backlink');
            } else {
                $backlink = URL::previous();
            }
            //================================================
            if (Session::has('tmp_choose')) {
                $totalprice = 0;
                $tmp_choose = Session::get('tmp_choose');
                foreach ($tmp_choose as $keydoc => $doc) {
                    $getdoc = Document::find($doc);
                    $totalprice += $getdoc['price'];
                }
                Session::forget('tmp_choose');
                if (Session::has('backlink')) {
                    $backlink = Session::get('backlink');
                    Session::forget('backlink');
                } else {
                    $backlink = URL::previous();
                }
                $responseCharge = $this->createCharge(($totalprice * 100), getenv('STRIPE_CURRENCY'), $customer_id, $data['description']);
                if (isset($responseCharge->status) && $responseCharge->status == "succeeded") {
                    $price = $totalprice;
                    $saveCharge = $this->saveTransactionToDatabase($responseCharge);
                    if ($saveCharge) {
                        foreach ($tmp_choose as $keydoc => $doc) {
                            $getdoc = Document::find($doc);
                            if ($this->savePaidToDatabase('document', $doc, $responseCharge->id)) {
                                $this->sendMailPaymentDocument($user, $getdoc['price']);
                                $check = true;
                            } else {
                                $check = false;
                                break;
                            }
                        }
                        if ($check) {
                            return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $backlink, 'status' => 1, 'message' => "Payment of " . $price . "$ was successful"]);
                        } else {
                            return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $backlink, 'status' => 0, 'message' => "Don't save information transaction"]);
                        }
                    } else {
                        return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $backlink, 'status' => 0, 'message' => "Don't save information transaction"]);
                    }
                } else {
                    return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $backlink, 'status' => 0, 'message' => "Payment failed ! Please, try again !"]);
                }
            } else {
                return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $backlink, 'status' => 0, 'message' => "Payment failed ! Please, try again !"]);
            }
        } else {
            return redirect()->route('getLoginUser')->with(['flash_level' => 'danger', 'flash_message' => "You need Login/Register to begin payment !"]);
        }
    }

    private function sendMailPaymentDocument($user, $price)
    {
        Mail::send('mails.paymentdocument', ['user' => $user, 'price' => $price], function ($message) use ($user, $price) {
            $message->from('vicoders.daily.report@gmail.com', 'Payment Confirmation');
            $message->to($user->email)->subject('Dear ' . $user->first_name);
        });
    }

    private function sendMailPaymentExam($user, $exam)
    {
        Mail::send('mails.paymentExam', ['user' => $user, 'exam' => $exam], function ($message) use ($user, $exam) {
            $message->from('vicoders.daily.report@gmail.com', 'Payment Confirmation');
            $message->to($user->email)->subject('Dear ' . $user->first_name);
        });
    }
    /**
     * [transactionpaid save data to database]
     * @param  [string] $type           [description]
     * @param  [int] $product_id     [description]
     * @param  [string] $transaction_id [id transaction when paid]
     * @return [boolean]                 [description]
     */
    public function savePaidToDatabase($type, $product_id, $transaction_id)
    {
        $current_user = Auth::user();
        $user = User::find($current_user->id);
        if (!empty($user)) {
            $paid = new Paid;
            $paid->user_id = $user['id'];
            $paid->product_id = $product_id;
            $paid->transaction_id = $transaction_id;
            $paid->type = $type;
            if ($paid->save()) {
                return true;
            } else {
                return false;
            }
        }
    }
    /**
     * [createCustomer description]
     * @param  [array] $data  [consist email and description]
     * @param  [type] $token [description]
     * @return [type]        [description]
     */
    public function createCustomer($data, $token)
    {
        try {
            $customer = \Stripe\Customer::create([
                "source" => $token,
                'email' => $data['email'],
                "description" => $data['description'],
            ]);
            return $customer;
        } catch (\Stripe\Error\Card $e) {
            // return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => $e . error . message]);
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => "Don't have create Stripe account !"]);
        }
    }
    /**
     * [getCustomer get inforation of custome execute billing]
     * @param  [type] $customer_id [description]
     * @return [type]              [description]
     */
    public function getCustomer($customer_id)
    {
        try {
            $response = \Stripe\Customer::retrieve($customer_id);
            return $response;
        } catch (\Stripe\Error\Card $e) {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => $e . error . message]);
        }
    }

    /**
     * [saveToDatabase description]
     * @param  [type] $customer [Response return when billing complete on Stripe]
     * @return [boolean]              [save complete so return true]
     */
    public function saveCustomerToDatabase($customer)
    {
        $current_user = Auth::user();
        $user = User::find($current_user->id);

        $user->stripe_id = $customer->id;
        if ($user->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * [charge billing for product by customer id]
     * @param  integer $price       price of product
     * @param  string  $currency    current, example : usd, vnd, ...
     * @param  [type]  $customer_id Customer_id is stripe_id
     * @param  string  $description can exist or not
     * @return [type]               return infor transaction when billing complete
     */
    public function createCharge($price, $currency = 'usd', $customer_id, $description = '')
    {
        try {
            $charge = \Stripe\Charge::create(array(
                "amount" => $price, // amount in cents
                "currency" => $currency,
                "description" => $description,
                "customer" => $customer_id,
            ));
            return $charge;
        } catch (\Stripe\Error\Card $e) {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Your card is cannot billing']);
        }
    }
    /**
     * [getIdCharge description]
     * @param  [type] $charge_id [description]
     * @return [type]            [description]
     */
    public function getCharge($charge_id)
    {
        try {
            $response = \Stripe\Charge::retrieve($charge_id);
        } catch (\Stripe\Error\Card $e) {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => $e . error . message]);
        }
        return $response;
    }

    /**
     * [saveTransactionToDatabase save information of customer transaction to database]
     * @param  [type] $charge [response result when billing complete]
     * @return [boolean]         [return true if save successful]
     */
    public function saveTransactionToDatabase($charge)
    {
        $current_user = Auth::user();
        $user = User::find($current_user->id);

        $subscription = new Subscription;
        $subscription->user_id = $user['id'];
        $subscription->name = $user['last_name'];
        $subscription->stripe_id = $charge->customer;
        $subscription->transaction_id = $charge->id;
        if ($subscription->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param  [type] $customer_id [description]
     * @return [boolean]              [return true so billing complete]
     */
    public function deleteCustomer($customer_id)
    {
        $user = User::where('customer_id', $customer_id)->get();
        $user->stripe_id = '';

        $subscription = Subscription::where('user_id', $user->id);
        $subscription->delete();

        $cu = \Stripe\Customer::retrieve($customer_id);
        $response = $cu->delete();
        if ($response->delete) {
            return true;
        } else {
            return false;
        }
    }

    public function switcherCurrencyAjax()
    {
        $amount = 100;
        $from = $_GET['from'];
        $to = $_GET['to'];
        $get = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=" . $from . "&to=" . $to);
        $get = explode("<span class=bld>", $get);
        $get = explode("</span>", $get[1]);
        $converted_amount = $get[0];
        // dd($converted_amount);
        return round($converted_amount, 2);
    }
}
