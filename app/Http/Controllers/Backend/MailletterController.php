<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Mailletter;
use App\Http\Controllers\Backend\AdminController;
use App\Http\Requests;
use Illuminate\Http\Request;

class MailletterController extends AdminController
{
    public function __construct() {
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email = Mailletter::first();
        return view('admin.mailletter.index', compact('email'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $first_email = Mailletter::first();
        if(empty($first_email))
        {
            $email = $request->all();
            $email = $request->except('_token');
            if(Mailletter::create($email)) {
                return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'success !! Update Successful']);
            }
            else
            {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error !! Update Faild']);
            }
        }
        else
        {
            $first_email -> email = $request->get('email');
            $first_email -> register = $request->get('register');
            $first_email -> reset_password = $request->get('reset_password');
            $first_email -> complete_exam = $request->get('complete_exam');
            $first_email -> subcribe = $request->get('subcribe');
            if($first_email->save())
            {
                return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'success !! Update Successful']);
            }
            else
            {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error !! Update Faild']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
