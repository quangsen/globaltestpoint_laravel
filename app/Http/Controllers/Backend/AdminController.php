<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Answer;
use App\Entities\Question;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileAdminRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use League\Csv\Reader;

class AdminController extends Controller {
	public function __construct() {
		$this->middleware('admin');
	}

	public function logout() {
		Auth::logout();
		return redirect('admin/login');
	}

	public function admin_dashboard() {
		return view('admin.dashboard');
	}

	public function showProfile() {
		$admin = Auth::user();
		$data  = [
			'title'      => 'Manage Profile',
			'tableTitle' => 'Manage Profile',
			'admin'      => $admin,
		];
		return view('admin.profile.profile', $data);
	}

	public function updateProfile(ProfileAdminRequest $request) {
		$data = $request->all();
		if ($data['password'] == '') {
			unset($data['password']);
		} else {
			$data['password'] = Hash::make($data['password']);
		}
		$admin = Auth::user();
		if ($admin == null) {
			Auth::logout();
			return redirect('admin/login');
		} else {
			$admin->update($data);
			return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success !! Update Profile']);
		}
	}

	public function ImportCsvToQuestionAnswer($csv_path, $exam_id) {
		if (!$csv_path) {
			throw new Exception("Can't found csv file", 1);
		}
		$csv = Reader::createFromPath($csv_path);
		// $header = $csv->setOffset(0);
		$resource = $csv->fetchAll();

		foreach ($resource as $key => $value) {

			// Ignore Header
			if ($key > 0) {

				// Remove empty value
				foreach ($resource[$key] as $key2 => $value2) {
					if ($value2 == '') {
						unset($resource[$key][$key2]);
					}
				}

				// Init variable
				$question   = $resource[$key][0];
				$answers    = array();
				$totalItem  = count($resource[$key]);
				$solution   = $resource[$key][$totalItem - 2];
				$trueAnswer = $resource[$key][$totalItem - 1];

				// Get list answer
				foreach ($resource[$key] as $key2 => $value2) {
					if ($key2 > 0 && $key2 < $totalItem - 2) {
						$answers[] = $resource[$key][$key2];
					}
				}

				// add Question to DB
				$questionDB           = new Question;
				$questionDB->exam_id  = $exam_id;
				$questionDB->question = $question;
				$questionDB->solution = $solution;
				$questionDB->save();
				$questionID = $questionDB->id;

				// add Answer to DB
				foreach ($answers as $kAnswer => $answer) {
					$answerDB              = new Answer;
					$answerDB->question_id = $questionID;
					$answerDB->answer      = $answer;
					if ($kAnswer + 1 == $trueAnswer) {
						$answerDB->is_correct = 1;
					} else {
						$answerDB->is_correct = 0;
					}
					$answerDB->save();
				}
			}
		}
	}
}
