<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Config;
use App\Http\Controllers\Backend\AdminController;
use Illuminate\Http\Request;

class MaintenanceController extends AdminController {
	public function index() {
		$config = Config::first();
		return view('admin.config.index', compact('config'));
	}

	public function maintenance(Request $request) {
		$data = $request->all();
		if (!$request->has('maintenance')) {
			$maintenance = (bool) 0;
		} else {
			$maintenance = (bool) $request->get('maintenance');
		}
		$config = Config::first();
		$config->maintenance = $maintenance;
		$config->save();
		return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Update successfully']);
	}
}
