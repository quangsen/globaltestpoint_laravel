<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Partner;
use App\Http\Controllers\Controller;

class PartnerController extends Controller
{
    public function listPartner() {
        return view('admin.partner.list', compact('partners'));
    }
}
