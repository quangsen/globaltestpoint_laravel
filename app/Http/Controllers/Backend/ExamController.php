<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Category;
use App\Entities\Exam;
use App\Http\Controllers\Backend\AdminController;
use App\Http\Requests\ExamRequest;

class ExamController extends AdminController {

	public function __construct() {
		parent::__construct();
	}

	public function getAdd() {
		$categories       = Category::where('type', 'Exam')->orderBy('order', 'desc')->orderBy('id', 'desc')->get();
		$categoryHasChild = $this->getChildCategory($categories);
		foreach ($categories as $key => $category) {
			if (in_array($category['id'], $categoryHasChild)) {
				unset($categories[$key]);
			}
		}
		return view('admin.exam.add', compact('categories'));
	}

	public function postAdd(ExamRequest $request) {
		$data = $request->all();
		if (!isset($data['active'])) {
			$data['active'] = 'no';
		}

		if ($request->get('grade_title')) {
			$grades     = array();
			$totalGrade = count($data['grade_title']);
			$gradeTitle = $data['grade_title'];
			$gradeMin   = $data['grade_min'];
			$gradeMax   = $data['grade_max'];
			for ($i = 0; $i < $totalGrade; $i++) {
				if ($gradeMin[$i] <= $gradeMax[$i]) {
					$grade = [
						'title' => $gradeTitle[$i],
						'min'   => $gradeMin[$i],
						'max'   => $gradeMax[$i],
					];
					$grades[] = $grade;
				}
			}
		}
		$data['grade'] = json_encode($grades);
		$exam          = Exam::create($data);
		return redirect()->route('admin.exam.getAdd')->with(['flash_level' => 'success', 'flash_message' => 'Success !! Add exams']);
	}

	public function lists() {
		$exams = Exam::orderBy('id', 'desc')->get();
		return view('admin.exam.lists', compact('exams'));
	}

	public function getDelete($id) {
		$exam = Exam::find($id);
		$exam->delete();
		return redirect()->route('admin.exam.lists')->with(['flash_level' => 'success', 'flash_message' => 'Success !! Delete Exams']);
	}

	public function getEdit($id) {
		$exam             = Exam::find($id);
		$categories       = Category::where('type', 'Exam')->orderBy('order', 'desc')->orderBy('id', 'desc')->get();
		$categoryHasChild = $this->getChildCategory($categories);
		foreach ($categories as $key => $category) {
			if (in_array($category['id'], $categoryHasChild)) {
				unset($categories[$key]);
			}
		}
		return view('admin.exam.edit', compact('exam', 'categories'));
	}

	public function postEdit($id, ExamRequest $request) {
		$data = $request->all();
		if (!isset($data['active'])) {
			$data['active'] = 'no';
		}

		if ($request->get('grade_title')) {
			$grades     = array();
			$totalGrade = count($data['grade_title']);
			$gradeTitle = $data['grade_title'];
			$gradeMin   = $data['grade_min'];
			$gradeMax   = $data['grade_max'];
			for ($i = 0; $i < $totalGrade; $i++) {
				if ($gradeMin[$i] <= $gradeMax[$i]) {
					$grade = [
						'title' => $gradeTitle[$i],
						'min'   => $gradeMin[$i],
						'max'   => $gradeMax[$i],
					];
					$grades[] = $grade;
				}
			}
		}
		$data['grade'] = json_encode($grades);
		$exam          = Exam::find($id)->update($data);
		return redirect()->route('admin.exam.lists')->with(['flash_level' => 'success', 'flash_message' => 'Success !! update Exams']);
	}
}
