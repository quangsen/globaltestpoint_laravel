<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Country;
use App\Entities\Role;
use App\Entities\User;
use App\Http\Controllers\Backend\AdminController;
use App\Http\Requests\UserRequest;
use Auth;

class UserController extends AdminController {

	public function __construct() {
		parent::__construct();
	}

	public function lists() {
		$users = User::where('id', '!=', Auth::user()->id)->get();
		$users = $users->map(function ($item) {
			$country = Country::find($item->country);
			if (!empty($country)) {
				$item->country = $country['name'];
			} else {
				$item->country = '';
			}
			return $item;
		});
		return view('admin.users.list', compact('users'));
	}

	public function getDelete($id) {
		$user = User::find($id);
		$user->delete($id);
		return redirect()->route('admin.users.list')->with(['flash_level' => 'success', 'flash_message' => 'Complete Delete User']);
	}

	public function create() {
		return view('admin.users.create');
	}

	public function getEdit($id) {
		$roles = Role::where('slug', '!=', 'admin')->get();
		$data  = User::with('roles')->findOrFail($id);
		return view('admin.users.edit', compact('data', 'roles'));
	}

	public function postEdit(UserRequest $request, $id) {
		$email_verified = 0;
		if ($request->active == null) {
			$active = 0;
		} else {
			$active         = 1;
			$email_verified = 1;
		}

		$role  = Role::find($request->role);
		$users = User::find($id);
		$users->first_name  = $request->first_name;
		$users->middle_name = $request->first_name;
		$users->last_name   = $request->last_name;
		$users->mobile      = $request->txtphone;
		$users->gender      = $request->txtsex;
		$users->address     = $request->txtaddress;
		if ($role != null && $role['slug'] == 'tester') {
			$users->active         = 1;
			$users->email_verified = 1;
		} else {
			$users->active         = $active;
			$users->email_verified = $email_verified;
		}
		$users->save();
		$attributes = [
			'role_id' => $request->role,
		];
		$userRoleID = $users->roles->first()['id'];
		$users->roles()->updateExistingPivot($userRoleID, $attributes);
		return redirect()->route('admin.users.list')->with(['flash_level' => 'success', 'flash_message' => 'success !! Complete Edit Users']);
	}
}
