<?php

namespace App\Http\Controllers\Backend;

use App\Entities\ExamResult;
use App\Entities\User;
use App\Http\Controllers\Backend\AdminController;
use App\Repositories\ExamResultRepository;
use Carbon\Carbon;

class ExamResultController extends AdminController {
	private $repository;
	public function __construct(ExamResultRepository $repository) {
		$this->repository = $repository;
		parent::__construct();
	}

	public function index() {
		$examResults = ExamResult::all();
		if (!$examResults->isEmpty()) {
			foreach ($examResults as $examResult) {
				$examResult['data'] = json_decode($examResult['data'], true);
				$userInstance       = User::find($examResult['user_id']);
				$examResult['user'] = $userInstance;

				$examResult['examDate'] = Carbon::parse($examResult['data']['exam']['created_at']['date']);
				$examResult['examDate'] = $examResult['examDate']->format('d/m/Y, h:i:s A');

				$examResult['timeTaken'] = $examResult['data']['timeTaken'] . ":000";
				$examResult['timeTaken'] = gmdate("H:i:s", (int) $examResult['timeTaken']);
			}
		}
		
		$data = [
			'title'       => 'Globaltestpoint - Exam Results',
			'tableTitle'  => 'Exam Results',
			'examResults' => $examResults,
		];
		return view('admin.exam_result.list', $data);
	}
}
