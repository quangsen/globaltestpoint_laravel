<?php

namespace App\Http\Controllers\API;

use App\Entities\Question;
use App\Http\Controllers\API\ApiController;
use App\Transformers\QuestionTransformer;

class QuestionController extends ApiController {
	public function show($id) {
		$question         = Question::findOrFail($id);
		$exam             = $question->exam;
		$question['exam'] = $exam;
		return $this->response->item($question, new QuestionTransformer);
	}
}
