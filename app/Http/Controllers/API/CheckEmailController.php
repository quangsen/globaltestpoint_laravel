<?php

namespace App\Http\Controllers\API;

use App\Entities\User;
use App\Http\Controllers\API\ApiController;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;

class CheckEmailController extends ApiController {
	public function checkEmailAdmin(Request $request) {
		$data = $request->only('email');
		if ($data['email'] == null) {
			return $this->response->error('Please enter an email.', 500);
		}
		$user = User::where('email', $data['email'])->get()->first();
		if ($user == null) {
			return $this->response->error("Email doesnot exist. Try again", 500);
		} else {
			return $this->response->item($user, new UserTransformer);
		}
	}
}
