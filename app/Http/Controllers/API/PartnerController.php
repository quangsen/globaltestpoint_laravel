<?php

namespace App\Http\Controllers\API;

use App\Entities\Partner;
use App\Entities\User;
use App\Events\AcceptNewPartner;
use App\Events\CancelPartner;
use App\Events\UserBecomePartner;
use App\Http\Controllers\API\ApiController;
use App\Transformers\PartnerTransformer;
use App\Validators\PartnerValidator;
use App\Validators\ValidatorInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;

class PartnerController extends ApiController
{
    /**
     * @var \App\Validators\ValidatorInterface
     */
    private $validator;

    public function __construct(PartnerValidator $validator)
    {
        $this->validator = $validator;
    }

    public function index(Request $request)
    {
        $partners = Partner::orderBy('id', 'desc')->get();
        return $this->collection($partners, new PartnerTransformer);
    }
    public function store(Request $request)
    {
        $partner = Partner::create($request->all());
        return $this->response->item($partner, new PartnerTransformer);
    }

    public function destroy($id)
    {
        $partner = Partner::findOrFail($id);
        if ($partner->delete()) {
            return $this->success();
        } else {
            throw new Exception("Can't delete this record", 1);
        }
    }
    public function active(Request $request, $id)
    {
        $this->validator->isValid($request, 'active');
        $partner = Partner::findOrFail($id);
        if ($request->get('accept')) {
            $user = User::where('email', $partner->email)->first();
            if (!isset($user)) {
                $user                 = User::create($partner->toArray());
                $password             = str_random(8);
                $user->password       = Hash::make($password);
                $user->email_verified = 1;
                Event::fire(new AcceptNewPartner($user, $password));
            } else {
                Event::fire(new UserBecomePartner($user));
            }
            $user->is_partner = 1;
            $user->save();
        } else {
            $user = User::where('email', $partner->email)->first();
            if (isset($user)) {
                $user->is_partner = 0;
                $user->save();
            }
            Event::fire(new CancelPartner($user));
        }
        $partner->accept = $request->get('accept');
        $partner->save();
        return $this->response->item($partner, new PartnerTransformer);
    }
}
