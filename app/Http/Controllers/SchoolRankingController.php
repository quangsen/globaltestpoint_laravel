<?php

namespace App\Http\Controllers;

use App\Entities\Country;
use App\Entities\SchoolDegree;
use App\Entities\SchoolType;
use App\Repositories\SchoolRepository;
use Illuminate\Http\Request;

class SchoolRankingController extends Controller {
	private $repository;

	public function __construct(SchoolRepository $repository) {
		$this->repository = $repository;
	}
	public function index(Request $request) {
		$countries      = Country::where('active', 1)->get();
		$school_degrees = SchoolDegree::where('active', 1)->get();
		$school_types   = SchoolType::where('active', 1)->get();
		$data           = [
			'countries'      => $countries,
			'school_degrees' => $school_degrees,
			'school_types'   => $school_types,
			'request'        => $request->all(),
		];

		$schools         = $this->repository->getSchools($request);
		$data['schools'] = $schools;
		return view('school_ranking.list', $data, [
			'schools' => $schools->appends($request->except('page')),
		]);
	}

	public function show($slug) {
		$school = $this->repository->findByField('slug', $slug)->first();
		$data   = [
			'school' => $school,
		];
		return view('school_ranking.detail', $data);
	}
}
