<?php

namespace App\Http\Controllers;

use App\Entities\Slider;
use App\Http\Controllers\Frontend\FrontendController;

class HomeController extends FrontendController {
	public function index() {
		$homeSliders = Slider::where('active', 1)->where('page', 1)->orderBy('sort', 'desc')->orderBy('id', 'desc')->get();
		return view('home/index', compact('homeSliders'));
	}
}
