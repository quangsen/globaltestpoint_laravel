<?php

namespace App\Http\Controllers;

use App\Exceptions\NotFoundException;
use App\Http\Requests\FileRequest;
use Illuminate\Http\Response;

class FileController extends Controller {

	public function upload(FileRequest $request) {
		$data = $request->only('file');
		if (!$request->hasFile('file')) {
			throw new NotFoundException("File");
		} else {
			$data = $request->all();
			$file = $request->file('file');
			$path = $this->_uploadFile($file, $data['folderUpload']);
			return $this->responseData($path);
		}
	}
}
