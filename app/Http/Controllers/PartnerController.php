<?php

namespace App\Http\Controllers;

use App\Entities\Country;
use App\Entities\Partner;
use App\Http\Controllers\Backend\AdminController;

class PartnerController extends Controller
{
    public function partner()
    {
        $countries = Country::with('states')->where('active', '1')->orderBy('order', 'desc')->get();
        return view('partner.partner', compact('countries'));
    }

    public function becomePartner()
    {
        return view('partner.become_partner');
    }

}
