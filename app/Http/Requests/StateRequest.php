<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StateRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'country_id' => 'required',
			'name'       => 'required|min:4|max:100',
			'order'      => 'max:10|regex: /^[0-9][0-9]*$/',
		];
	}

	public function messages() {
		return [
			'country_id.required' => 'The country field is required',
			'order.regex'         => 'Order is unsigned',
		];
	}
}
