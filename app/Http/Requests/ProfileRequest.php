<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfileRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'fname'     => 'required|max:40',
			'gender'    => 'required|regex:/^[0-9]{1}$/i',
			'ulocation' => 'required',
		];
	}

	public function messages() {
		return [
			'fname.required'     => 'The first name field is required.',
			'gender.required'    => 'The gender field is required.',
			'ulocation.required' => 'The localtion field is required.',
		];
	}
}
