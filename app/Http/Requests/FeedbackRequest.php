<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FeedbackRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname' => 'required',
            'message'  => 'required',
            'subject'  => 'required',
            'email' => 'required|email',
            'g-recaptcha-response' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required'     => 'The :attribute field is required.',
            'email'                => 'The Email field format is invalid.',
            'g-recaptcha-response.required' => 'The Recaptcha is required.',
        ];
    }
}
