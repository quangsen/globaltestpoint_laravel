<?php

namespace App\Entities;

use App\Entities\Country;
use App\Entities\State;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Partner extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'first_name',
        'last_name',
        'business_name',
        'address',
        'about',
        'city',
        'email',
        'country_id',
        'state_id',
        'review_links',
        'website',
        'how_did_here',
        'how_do_intend_to_reach',
        'interest',
        'number_of_users',
        'target',
        'perference',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

}
