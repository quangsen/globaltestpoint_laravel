<?php

namespace App\Entities;

use App\Entities\Exam;
use App\Entities\Paid;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Bican\Roles\Traits\HasRoleAndPermission;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Prettus\Repository\Traits\TransformableTrait;

class User extends Authenticatable implements HasRoleAndPermissionContract, CanResetPassword
{
    use TransformableTrait;
    use HasRoleAndPermission;
    use Billable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'image', 'mobile', 'birth', 'sumary', 'address', 'gender', 'city', 'country', 'active', 'email_verified', 'remember_token', '  created_at', '  updated_at',
    ];

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function exan_user()
    {
        return $this->hasMany('App\Entities\User');
    }

    /**
     * [ads return infor User reference to Ads]
     * @return [type] [description]
     */
    public function ads()
    {
        return $this->hasMany(Ads::class);
    }

    public function exams()
    {
        return $this->belongsToMany(Exam::class);
    }

    public function paid_exams()
    {
        return $this->hasMany(Paid::class);
    }
}
