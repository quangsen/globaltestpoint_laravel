<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Mailletter extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'mailletters';

    protected $fillable = ['id', 'email', 'register', 'reset_password', 'complete_exam', 'subcribe', 'created_at', 'updated_at'];
}
