<?php

namespace App\Entities;

use App\Entities\Post;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Link extends Model implements Transformable
{
	use TransformableTrait;
    protected $table = 'links';

    protected $fillable = ['id', 'url', 'position', 'post_id', 'order', 'active', 'created_at', 'updated_at'];
    /**
     * [post return data into relation post table]
     * @return [object] [description]
     */
    public function post()
    {
    	return $this->belongsTo(Post::class, 'post_id');
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
