<?php

namespace App\Entities;

use App\Entities\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Paid extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'transactionpaid';

    protected $fillable = ['id', 'user_id', 'product_id', 'transaction_id', 'type', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
