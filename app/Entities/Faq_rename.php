<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Faq extends Model implements Transformable
{
    use TransformableTrait;
    protected $table = 'faqs';

    protected $fillable = ['id', 'question', 'answer'];
}
