<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public $table = 'options';

    protected $fillable = ['id', 'logo', 'title', 'image_login', 'image_register', 'image_login_register', 'email', 'termpolicy', 'phone', 'address', 'fax', 'facebook', 'twitter', 'created_at', 'updated_at', 'introduction_book', 'introduction_variousexam', 'introduction_exams'];
}
