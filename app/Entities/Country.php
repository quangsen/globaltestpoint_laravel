<?php

namespace App\Entities;

use App\Entities\Partner;
use App\Entities\School;
use App\Entities\State;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Country extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['id', 'name', 'order', 'active'];

    public function states()
    {
        return $this->hasMany(State::class);
    }

    public function schools()
    {
        return $this->hasManyThrough(School::class, State::class);
    }

    public function partners()
    {
        return $this->hasMany(Partner::class, 'country_id');
    }

    public function setNameAttribute($value) {
        $this->attributes['name'] = $value;
        $slug                     = str_slug($value);
        $i                        = 1;
        while (!$this->where('slug', $slug)->get()->isEmpty()) {
            $slug = str_slug($value) . '-' . $i++;
        }
        $this->attributes['slug'] = $slug;
    }
}
