<?php

namespace App\Providers;

use App\Entities\Category;
use App\Entities\Option;
use App\Entities\SchoolDegree;
use App\Repositories\CategoriesRepository;
use App\Repositories\CategoriesRepositoryEloquent;
use App\Repositories\ExamResultRepository;
use App\Repositories\ExamResultRepositoryEloquent;
use App\Repositories\UserRepository;
use App\Repositories\UserRepositoryEloquent;
use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot() {
		if (!app()->runningInConsole()) {

			$img_logo = Option::first();

			$categories = Category::where('page_id', 2)->orderBy('order', 'desc')->orderBy('id', 'desc')->get();
			if (!$categories->isEmpty()) {

				// get Category has child and no child
				$categoryHasChild = array();
				foreach ($categories as $key => $category) {
					if ($category['type'] != 'blank-page') {
						if ($category['parent_id'] != 0) {
							if (!in_array($category['parent_id'], $categoryHasChild)) {
								$categoryHasChild[] = $category['parent_id'];
							}
						}
					} elseif ($category['type'] == 'blank-page') {
						if (!$category->posts->isEmpty()) {
							if (!in_array($category['parent_id'], $categoryHasChild)) {
								$categoryHasChild[] = $category['id'];
							}
						}
					}
				}
				foreach ($categories as $key => $category) {
					if (in_array($category['id'], $categoryHasChild)) {
						$categories[$key]['hasChild'] = true;
					} else {
						$categories[$key]['hasChild'] = false;
					}
				}
				$category_parents = $categories->filter(function ($item) {
					return $item->parent_id == 0;
				});
				View()->share('category_parents', $category_parents);
				// end get Category child
			}

			$school_degrees = SchoolDegree::where('active', 1)->orderBy('order', 'desc')->orderBy('id', 'desc')->get();
			View()->share('school_degrees', $school_degrees);
			View()->share('categories', $categories);
			View()->share('img_logo', $img_logo);
		}
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->bind(UserRepository::class, UserRepositoryEloquent::class);
		$this->app->bind(CategoriesRepository::class, CategoriesRepositoryEloquent::class);
		$this->app->bind(\App\Repositories\ExamRepository::class, \App\Repositories\ExamRepositoryEloquent::class);
		$this->app->bind(\App\Repositories\ExamResultRepository::class, \App\Repositories\ExamResultRepositoryEloquent::class);
		$this->app->bind(\App\Repositories\CountryRepository::class, \App\Repositories\CountryRepositoryEloquent::class);
		$this->app->bind(\App\Repositories\StateRepository::class, \App\Repositories\StateRepositoryEloquent::class);
		$this->app->bind(\App\Repositories\SchoolTypeRepository::class, \App\Repositories\SchoolTypeRepositoryEloquent::class);
		$this->app->bind(\App\Repositories\SchoolDegreeRepository::class, \App\Repositories\SchoolDegreeRepositoryEloquent::class);
		$this->app->bind(\App\Repositories\SchoolRepository::class, \App\Repositories\SchoolRepositoryEloquent::class);
		$this->app->bind(\App\Repositories\DocumentRepository::class, \App\Repositories\DocumentRepositoryEloquent::class);
		$this->app->bind(\App\Repositories\FaqRepository::class, \App\Repositories\FaqRepositoryEloquent::class);
	}
}
