<?php

namespace App\Providers;

use App\Events\AcceptNewPartner;
use App\Events\CancelPartner;
use App\Events\UserBecomePartner;
use App\Listeners\AcceptNewPartnerListener;
use App\Listeners\CancelPartnerListener;
use App\Listeners\UserBecomePartnerListener;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        AcceptNewPartner::class  => [
            AcceptNewPartnerListener::class,
        ],
        UserBecomePartner::class => [
            UserBecomePartnerListener::class,
        ],
        CancelPartner::class     => [
            CancelPartnerListener::class,
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
