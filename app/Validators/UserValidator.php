<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class UserValidator extends AbstractValidator
{

    protected $rules = [
        'login'    => [
            'email'    => ['email', 'required'],
            'password' => ['required', 'min:6', 'max:20'],
        ],
        'register' => [
            'email'    => ['email', 'required'],
            'password' => ['required', 'min:6', 'max:20'],
        ],
    ];
}
