<?php

namespace App\Transformers;

use App\Entities\State;
use League\Fractal\TransformerAbstract;

/**
 * Class StateTransformer
 * @package namespace App\Transformers;
 */
class StateTransformer extends TransformerAbstract
{

    /**
     * Transform the \State entity
     * @param \State $model
     *
     * @return array
     */
    public function transform(State $model)
    {
        return [
            'id'         => (int) $model->id,

            'name'       => $model->name,

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }
}
