<?php

namespace App\Transformers;

use App\Entities\User;
use League\Fractal\TransformerAbstract;

/**
 * Class ExamTransformer
 * @package namespace App\Transformers;
 */
class UserTransformer extends TransformerAbstract {

	/**
	 * Transform the \Exam entity
	 * @param \Exam $model
	 *
	 * @return array
	 */
	public function transform(User $model) {

		return [
			'id'             => (int) $model->id,
			'first_name'     => $model->first_name,
			'middle_name'    => $model->middle_name,
			'last_name'      => $model->last_name,
			'email'          => $model->email,
			'image'          => $model->image,
			'mobile'         => $model->mobile,
			'birth'          => $model->birth,
			'user_type'      => $model->user_type,
			'sumary'         => $model->sumary,
			'address'        => $model->address,
			'gender'         => $model->gender,
			'city'           => $model->city,
			'country'        => $model->country,
			'active'         => $model->active,
			'email_verified' => $model->email_verified,
			'created_at'     => $model->created_at,
			'updated_at'     => $model->updated_at,
		];
	}
}
