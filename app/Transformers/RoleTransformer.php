<?php

namespace App\Transformers;

use App\Entities\Role;
use League\Fractal\TransformerAbstract;

/**
 * Class ExamTransformer
 * @package namespace App\Transformers;
 */
class RoleTransformer extends TransformerAbstract {

	/**
	 * Transform the \Exam entity
	 * @param \Exam $model
	 *
	 * @return array
	 */
	public function transform(Role $model) {

		return [
			'id'         => (int) $model->id,
			'name'       => $model->name,
			'slug'       => $model->slug,
			'created_at' => $model->created_at,
			'updated_at' => $model->updated_at,
		];
	}
}
