<?php

namespace App\Transformers;

use App\Entities\Question;
use App\Transformers\AnswerTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class QuestionTransformer
 * @package namespace App\Transformers;
 */
class QuestionTransformer extends TransformerAbstract {
	protected $defaultIncludes = ['answers'];
	/**
	 * Transform the \Question entity
	 * @param \Question $model
	 *
	 * @return array
	 */
	public function transform(Question $model) {
		return [
			'id'       => (int) $model->id,
			'question' => $model->question,
			'solution' => $model->solution,
			'type'     => $model->type,
			'exam'     => $model->exam,
			'order'    => (int) $model->order,
		];
	}
	public function includeAnswers(Question $model) {
		return $this->collection($model->answers, new AnswerTransformer);
	}
}
