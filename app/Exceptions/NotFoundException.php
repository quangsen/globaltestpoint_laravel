<?php

namespace App\Exceptions;

class NotFoundException extends \Exception
{
    public function __construct($entity = null)
    {
        if ($entity) {
            $this->message = $entity . ' not found';
        } else {
            $this->message = 'Not Found';
        }
        $this->error_code = 1007;
    }
}
