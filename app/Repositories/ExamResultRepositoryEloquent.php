<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ExamResultRepository;
use App\Entities\ExamResult;
use App\Validators\ExamResultValidator;

/**
 * Class ExamResultRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ExamResultRepositoryEloquent extends BaseRepository implements ExamResultRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ExamResult::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
