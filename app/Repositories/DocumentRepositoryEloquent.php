<?php

namespace App\Repositories;

use App\Entities\Category;
use App\Entities\Document;
use App\Repositories\DocumentRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class DocumentRepositoryEloquent
 * @package namespace App\Repositories;
 */
class DocumentRepositoryEloquent extends BaseRepository implements DocumentRepository {
	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	public function model() {
		return Document::class;
	}

	/**
	 * Boot up the repository, pushing criteria
	 */
	public function boot() {
		$this->pushCriteria(app(RequestCriteria::class));
	}

	public function getCategory() {
		return Category::where('type', 'document')->where('active', '1')->where('parent_id', '!=', 0)->get();
	}

	public function getCategoryOfDocument($document_id) {
		$document = Document::find($document_id);
		return $document->category;
	}
}
