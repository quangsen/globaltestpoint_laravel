<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ExamUserRepository
 * @package namespace App\Repositories;
 */
interface ExamUserRepository extends RepositoryInterface
{
    //
}
