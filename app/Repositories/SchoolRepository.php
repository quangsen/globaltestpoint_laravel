<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SchoolRepository
 * @package namespace App\Repositories;
 */
interface SchoolRepository extends RepositoryInterface {
	public function getAll();
	public function getStateOfSchool($school_id);
	public function getCountries();
	public function getStates();
	public function getSchoolTypes();
	public function getSchoolDegrees();
	public function getSchools($request);
}
