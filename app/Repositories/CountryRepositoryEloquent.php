<?php

namespace App\Repositories;

use App\Entities\Country;
use App\Repositories\CountryRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class CountryRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CountryRepositoryEloquent extends BaseRepository implements CountryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Country::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getAll() {
        $countries = Country::orderBy('order', 'desc')->orderBy('id', 'desc')->get();
        return $countries;
    }
}
