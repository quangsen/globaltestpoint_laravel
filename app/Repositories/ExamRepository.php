<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ExamRepository
 * @package namespace App\Repositories;
 */
interface ExamRepository extends RepositoryInterface {
	public function getAll();
	public function getExamByCategory($category_id);
	public function getExam($exam_id);
	public function getQuestions($exam_id);
	public function checkBuyExam($user_id, $exam_id);
	public function getResultExam($exam);
	public function getAnswerCorrect($questions);
	public function getYourChoice($questions, $bindingQuestion, $alphas);
	public function saveExamToDB($user, $examResult);
	public function sendMail($user, $exam);
}
