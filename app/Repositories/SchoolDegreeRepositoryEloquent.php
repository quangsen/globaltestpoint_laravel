<?php

namespace App\Repositories;

use App\Entities\SchoolDegree;
use App\Repositories\SchoolDegreeRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class SchoolDegreeRepositoryEloquent
 * @package namespace App\Repositories;
 */
class SchoolDegreeRepositoryEloquent extends BaseRepository implements SchoolDegreeRepository {
	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	public function model() {
		return SchoolDegree::class;
	}

	/**
	 * Boot up the repository, pushing criteria
	 */
	public function boot() {
		$this->pushCriteria(app(RequestCriteria::class));
	}

	public function getAll() {
		$school_degrees = SchoolDegree::orderBy('order', 'desc')->orderBy('id', 'desc')->get();
		return $school_degrees;
	}
}
