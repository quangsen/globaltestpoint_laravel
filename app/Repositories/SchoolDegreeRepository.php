<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SchoolDegreeRepository
 * @package namespace App\Repositories;
 */
interface SchoolDegreeRepository extends RepositoryInterface
{
    public function getAll();
}
