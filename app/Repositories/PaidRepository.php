<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PaidRepository
 * @package namespace App\Repositories;
 */
interface PaidRepository extends RepositoryInterface
{
    //
}
