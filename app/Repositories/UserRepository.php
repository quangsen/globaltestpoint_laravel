<?php

namespace App\Repositories;

use Composer\Autoload\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace App\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    //
}
