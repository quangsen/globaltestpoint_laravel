<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoriesRepository
 * @package namespace App\Repositories;
 */
interface CategoriesRepository extends RepositoryInterface {
	public function getAll();
	public function getOne($cate_id);
	public function createCategory($data = []);
	public function updateCategory($data = [], $cate_id);
	public function deleteCategory($cate_id);
	public function getChildCategory($category_id);
}
