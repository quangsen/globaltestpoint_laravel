<?php

namespace App\Listeners;

use App\Events\AcceptNewPartner;
use Illuminate\Support\Facades\Mail;

class AcceptNewPartnerListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AcceptNewPartner  $event
     * @return void
     */
    public function handle(AcceptNewPartner $event)
    {
        $user     = $event->user;
        $password = $event->password;
        Mail::send('mails.new_partner_was_accepted', ['user' => $user, 'password' => $password], function ($message) use ($user) {
            $message->from('vicoders.daily.report@gmail.com', 'Globaltestpoint');
            $message->to($user->email)->subject('Your application has been accept! globaltestpoint');
        });
    }
}
