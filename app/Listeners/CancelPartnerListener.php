<?php

namespace App\Listeners;

use App\Events\CancelPartner;
use Illuminate\Support\Facades\Mail;

class CancelPartnerListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CancelPartner  $event
     * @return void
     */
    public function handle(CancelPartner $event)
    {
        $user = $event->user;
        Mail::send('mails.cancel_partner', ['user' => $user], function ($message) use ($user) {
            $message->from('vicoders.daily.report@gmail.com', 'Globaltestpoint');
            $message->to($user->email)->subject('Your partner account has been cancel! globaltestpoint');
        });
    }
}
