<?php

namespace App\Listeners;

use App\Events\UserBecomePartner;
use Illuminate\Support\Facades\Mail;

class UserBecomePartnerListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserBecomePartner  $event
     * @return void
     */
    public function handle(UserBecomePartner $event)
    {
        $user = $event->user;
        Mail::send('mails.user_become_partner', ['user' => $user], function ($message) use ($user) {
            $message->from('vicoders.daily.report@gmail.com', 'Globaltestpoint');
            $message->to($user->email)->subject('Your application has been accept! globaltestpoint');
        });
    }
}
