<?php

use App\Entities\Country;
use App\Entities\State;
use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$totalCountry = Country::count();
		for ($i = 1; $i <= 60; $i++) {
			$country_id = (int) fmod($i, $totalCountry);
			if ($country_id == 0) {
				$country_id = $totalCountry;
			}

			$faker = new Faker\Generator();
			$faker->addProvider(new Faker\Provider\en_US\Address($faker));
			$state             = new State;
			$state->name       = $faker->state();
			$state->country_id = $country_id;
			$state->order      = 0;
			$state->active     = 0;
			$state->save();
		}
	}
}
