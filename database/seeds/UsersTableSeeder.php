<?php

use App\Entities\User;
use Bican\Roles\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$adminRole  = Role::find(1);
		$userRole   = Role::find(2);
		$testerRole = Role::find(3);
		$users      = [
			'admin'   => [
				'user_type' => 'student',
				'email'     => 'admin@gmail.com',
				'role'      => 'adminRole',
				'active'         => 1,
				'email_verified' => 1,
			],
			'chungnd' => [
				'user_type'      => 'tutor',
				'email'          => 'nguyenchung26011992@gmail.com',
				'role'           => 'userRole',
				'active'         => 1,
				'email_verified' => 1,
			],
			'tester1' => [
				'user_type'      => 'student',
				'email'          => 'honglien94.pt@gmail.com',
				'role'           => 'testerRole',
				'active'         => 1,
				'email_verified' => 1,
			],
			'tester2' => [
				'user_type'      => 'student',
				'email'          => 'nguyenloi296@gmail.com',
				'role'           => 'testerRole',
				'active'         => 1,
				'email_verified' => 1,
			],
			'tester3' => [
				'user_type'      => 'student',
				'email'          => 'dohueatk@gmail.com',
				'role'           => 'testerRole',
				'active'         => 1,
				'email_verified' => 1,
			],
			'user'    => [
				'user_type' => 'student',
				'email'     => 'user@gmail.com',
				'role'      => 'userRole',
				'active'         => 1,
				'email_verified' => 1,
			],
		];
		foreach ($users as $u) {
			$faker = new Faker\Generator();
			$faker->addProvider(new Faker\Provider\en_US\Person($faker));
			$faker->addProvider(new Faker\Provider\en_US\Address($faker));
			$faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));
			$faker->addProvider(new Faker\Provider\en_US\Company($faker));
			$faker->addProvider(new Faker\Provider\DateTime($faker));
			$faker->addProvider(new Faker\Provider\Internet($faker));
			$faker->addProvider(new Faker\Provider\Lorem($faker));
			$user         = new User;
			$user->email  = $u['email'];
			$user->gender = mt_rand(0, 1);

			if ($user->gender == 0) {
				$gender = 'male';
			} else {
				$gender = 'female';
			}
			$user->first_name     = $faker->firstName($gender);
			$user->last_name      = $faker->lastName($gender);
			$user->password       = Hash::make('secret');
			$user->image          = '/uploads/default_avatar.png';
			$user->mobile         = $faker->PhoneNumber();
			$user->sumary         = $faker->text();
			$user->birth          = $faker->dateTimeThisCentury->format('Y-m-d');
			$user->address        = $faker->address();
			$user->city           = $faker->city();
			$user->country        = mt_rand(1, 2);
			$user->user_type      = $u['user_type'];
			$user->active         = $u['active'];
			$user->email_verified = $u['email_verified'];
			$user->save();
			$user->attachRole(${$u['role']});
		}
		for ($i = 0; $i < 5; $i++) {
			$faker = new Faker\Generator();
			$faker->addProvider(new Faker\Provider\en_US\Person($faker));
			$faker->addProvider(new Faker\Provider\en_US\Address($faker));
			$faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));
			$faker->addProvider(new Faker\Provider\en_US\Company($faker));
			$faker->addProvider(new Faker\Provider\DateTime($faker));
			$faker->addProvider(new Faker\Provider\Internet($faker));
			$faker->addProvider(new Faker\Provider\Lorem($faker));
			$user         = new User;
			$user->email  = $faker->email;
			$user->gender = mt_rand(0, 1);

			if ($user->gender == 0) {
				$gender = 'male';
			} else {
				$gender = 'female';
			}
			$user->first_name     = $faker->firstName($gender);
			$user->last_name      = $faker->lastName($gender);
			$user->password       = Hash::make('secret');
			$user->image          = '/uploads/default_avatar.png';
			$user->mobile         = $faker->PhoneNumber();
			$user->sumary         = $faker->text();
			$user->birth          = $faker->dateTimeThisCentury->format('Y-m-d');
			$user->address        = $faker->address();
			$user->city           = $faker->city();
			$user->country        = mt_rand(1, 2);
			$user->user_type      = 'student';
			$user->active         = mt_rand(0, 1);
			$user->email_verified = $user->active;
			$user->save();
			$user->attachRole($userRole);
		}
	}

}
