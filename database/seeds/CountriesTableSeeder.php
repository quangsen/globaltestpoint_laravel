<?php

use App\Entities\Country;
use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = new Faker\Generator();
		$faker->addProvider(new Faker\Provider\en_US\Address($faker));

		$countries = [
			[
				'name' => 'United Kingdom',
			],
			[
				'name' => 'United States',
			],
			[
				'name' => 'United States',
			],
		];
		foreach ($countries as $item) {
			$data = array(
				'name'   => $item['name'],
				'order'  => 0,
				'active' => 1,
			);
			Country::create($data);
		}

		for ($i = 1; $i <= 18; $i++) {
			$data = array(
				'name'   => $faker->country(),
				'order'  => 0,
				'active' => mt_rand(0, 1),
			);
			Country::create($data);
		}
	}
}
