<?php

use App\Entities\Option;
use Illuminate\Database\Seeder;

class OptionTableSeeder extends Seeder
{
    public function run()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new Faker\Provider\DateTime($faker));
        $faker->addProvider(new Faker\Provider\Internet($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));
        $faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));

        $option           = new Option;
        $option->title    = "Testyourself - Globalteslpoint";
        $option->page     = 0;
        $option->email    = "demo@contact.com";
        $option->phone    = $faker->PhoneNumber();
        $option->logo     = '/assets/frontend/images/testyourself/logo.png';
        $option->address  = "Hanoi/Vietnam";
        $option->fax      = $faker->PhoneNumber();
        $option->facebook = "http://www.facebook.com/vicoders";
        $option->twitter  = "http://www.twitter.com/";
        $option->image_login = '/assets/frontend/images/testyourself/logo.png';
        $option->image_register = '/assets/frontend/images/testyourself/logo.png';
        $option->image_login_register = '/assets/frontend/images/testyourself/logo.png';
        $option->save();

        $option2           = new Option;
        $option2->title    = "What Do You Want Done - Globalteslpoint";
        $option2->page     = 1;
        $option2->email    = "demo@contact.com";
        $option2->phone    = $faker->PhoneNumber();
        $option2->logo     = '/assets/frontend/images/testyourself/logo.png';
        $option2->address  = "Hanoi/Vietnam";
        $option2->fax      = $faker->PhoneNumber();
        $option2->facebook = "http://www.facebook.com/vicoders";
        $option2->twitter  = "http://www.twitter.com/";
        $option2->image_login = '';
        $option2->image_register = '';
        $option2->image_login_register = '';
        $option2->save();
    }
}
