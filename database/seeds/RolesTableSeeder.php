<?php

use Bican\Roles\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::create([
            'name'        => 'Admin',
            'slug'        => 'admin',
        ]);

        $userRole = Role::create([
            'name' => 'User',
            'slug' => 'user',
        ]);

        $testerRole = Role::create([
            'name' => 'Tester',
            'slug' => 'tester',
        ]);
    }
}
