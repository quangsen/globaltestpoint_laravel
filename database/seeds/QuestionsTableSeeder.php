<?php

use App\Entities\Answer;
use App\Entities\Question;
use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		// Create array include 10 question
		$questions = [
			[
				'question' => 'uploads/questions/bear.jpg',
				'type'     => 2,
				'answers'  => [
					[
						'answer'     => 'bear',
						'is_correct' => 1,
					],
					[
						'answer'     => 'elephant',
						'is_correct' => 0,
					],
					[
						'answer'     => 'giraffe',
						'is_correct' => 0,
					],
					[
						'answer'     => 'tiger',
						'is_correct' => 0,
					],
					[
						'answer'     => 'crocodile',
						'is_correct' => 0,
					],
					[
						'answer'     => 'monkey',
						'is_correct' => 0,
					],
				],
			],
			[
				'question' => 'uploads/questions/elephant.jpg',
				'type'     => 2,
				'answers'  => [
					[
						'answer'     => 'bear',
						'is_correct' => 0,
					],
					[
						'answer'     => 'elephant',
						'is_correct' => 1,
					],
					[
						'answer'     => 'giraffe',
						'is_correct' => 0,
					],
					[
						'answer'     => 'tiger',
						'is_correct' => 0,
					],
					[
						'answer'     => 'crocodile',
						'is_correct' => 0,
					],
					[
						'answer'     => 'monkey',
						'is_correct' => 0,
					],
				],
			], [
				'question' => 'uploads/questions/giraffe.jpg',
				'type'     => 2,
				'answers'  => [
					[
						'answer'     => 'bear',
						'is_correct' => 0,
					],
					[
						'answer'     => 'elephant',
						'is_correct' => 0,
					],
					[
						'answer'     => 'giraffe',
						'is_correct' => 1,
					],
					[
						'answer'     => 'tiger',
						'is_correct' => 0,
					],
					[
						'answer'     => 'crocodile',
						'is_correct' => 0,
					],
					[
						'answer'     => 'monkey',
						'is_correct' => 0,
					],
				],
			], [
				'question' => 'uploads/questions/tiger.jpg',
				'type'     => 2,
				'answers'  => [
					[
						'answer'     => 'bear',
						'is_correct' => 0,
					],
					[
						'answer'     => 'elephant',
						'is_correct' => 0,
					],
					[
						'answer'     => 'giraffe',
						'is_correct' => 0,
					],
					[
						'answer'     => 'tiger',
						'is_correct' => 1,
					],
					[
						'answer'     => 'crocodile',
						'is_correct' => 0,
					],
					[
						'answer'     => 'monkey',
						'is_correct' => 0,
					],
				],
			], [
				'question' => 'uploads/questions/crocodile.jpg',
				'type'     => 2,
				'answers'  => [
					[
						'answer'     => 'bear',
						'is_correct' => 0,
					],
					[
						'answer'     => 'elephant',
						'is_correct' => 0,
					],
					[
						'answer'     => 'giraffe',
						'is_correct' => 0,
					],
					[
						'answer'     => 'tiger',
						'is_correct' => 0,
					],
					[
						'answer'     => 'crocodile',
						'is_correct' => 1,
					],
					[
						'answer'     => 'monkey',
						'is_correct' => 0,
					],
				],
			], [
				'question' => 'uploads/questions/monkey.jpg',
				'type'     => 2,
				'answers'  => [
					[
						'answer'     => 'bear',
						'is_correct' => 0,
					],
					[
						'answer'     => 'elephant',
						'is_correct' => 0,
					],
					[
						'answer'     => 'giraffe',
						'is_correct' => 0,
					],
					[
						'answer'     => 'tiger',
						'is_correct' => 0,
					],
					[
						'answer'     => 'crocodile',
						'is_correct' => 0,
					],
					[
						'answer'     => 'monkey',
						'is_correct' => 1,
					],
				],
			],
		];

		for ($i = 1; $i <= 4; $i++) {
			$question = [
				'question' => "$i + $i = ?",
				'type'     => 1,
			];
			$answers = array();
			for ($j = 1; $j <= 4; $j++) {
				$answer = [
					'answer'     => ($j + 1) * $i,
					'is_correct' => $j == 1 ? 1 : 0,
				];
				$answers[] = $answer;
			}
			$question['answers'] = $answers;
			$questions[]         = $question;
		}
		// End create array include 10 question

		/**
		 * Create random question and answer in 10 exists question for exam
		 * with id from 1 to 5
		 */
		for ($i = 1; $i <= 5; $i++) {
			$checkDupplicate = [];
			$totalQuestions  = count($questions);
			for ($j = 1; $j <= $totalQuestions; $j++) {
				$dupplicate = false;
				while (!$dupplicate) {
					$rdNumber = mt_rand(0, 9);
					if (!in_array($rdNumber, $checkDupplicate)) {
						$dupplicate        = true;
						$checkDupplicate[] = $rdNumber;

						$questionDB           = new Question;
						$questionDB->question = $questions[$rdNumber]['question'];
						$questionDB->type     = $questions[$rdNumber]['type'];
						$questionDB->exam_id  = $i;
						$questionDB->order    = 0;
						$questionDB->save();

						foreach ($questions[$rdNumber]['answers'] as $answer) {
							$answerDB              = new Answer;
							$answerDB->answer      = $answer['answer'];
							$answerDB->is_correct  = $answer['is_correct'];
							$answerDB->question_id = (($i - 1) * 10) + $j;
							$answerDB->save();
						}
					}
				}
			}
		}
		// End create question
	}
}
