<?php

use App\Entities\Type;
use Illuminate\Database\Seeder;


class TypeTableSeeder extends Seeder
{
    public function run()
    {
        $types = [
            [
                'name' => 'Post',
            ],
            [
                'name' => 'Video',
            ],
            [
                'name' => 'Exam',
            ],
            [
                'name' => 'document',
            ],
            [
                'name' => 'blank-page',
            ],
        ];
        foreach ($types as $item) {
            $type = new Type;
            $type->name = $item['name'];
            $type->save();
        }
    }
}
