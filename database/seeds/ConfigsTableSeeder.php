<?php

use App\Entities\Config;
use Illuminate\Database\Seeder;

class ConfigsTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$config              = new Config;
		$config->maintenance = (bool) 0;
		$config->save();
	}
}
