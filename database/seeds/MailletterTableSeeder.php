<?php

use App\Entities\Mailletter;
use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory as TestDummy;

class MailletterTableSeeder extends Seeder
{
    public function run()
    {
        $email = new Mailletter;
		$email->email = "vicoders.daily.report@gmail.com";
		$email->register = "Thank for register";
		$email->reset_password = "Comfirm change password. New password is ...";
		$email->complete_exam = "Congratulations ! You pass this exam !";
		$email->subcribe = "You registered receive mail from us";
		$email->save();
    }
}
