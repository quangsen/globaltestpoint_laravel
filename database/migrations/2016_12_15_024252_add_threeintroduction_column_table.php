<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThreeintroductionColumnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('options', function(Blueprint $table){
            $table->text('introduction_book')->nullable();
            $table->text('introduction_variousexam')->nullable();
            $table->text('introduction_exams')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('options', function(Blueprint $table){
            $table->dropColumn('introduction_book');
            $table->dropColumn('introduction_variousexam');
            $table->dropColumn('introduction_exams');
        });
    }
}
