<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->increments('id');
            $table->text('leftlink');
            $table->text('rightlink');
            $table->text('sublink');
            $table->string('position', 50);
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
            $table->string('image');
            $table->string('image_cover');
            $table->integer('order');
            $table->integer('parent_cate_id')->unsigned()->default(0);
            $table->integer('page')->comment('0: testyourself; 1: Youwantdone');
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('links');
    }
}
