<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailletterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailletters', function(Blueprint $table){
            $table->increments('id');
            $table->string('email');
            $table->string('register');
            $table->string('reset_password');
            $table->string('complete_exam');
            $table->string('subcribe');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mailletters');
    }
}
